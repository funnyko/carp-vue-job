/*
  import ...
  ...
  ...
  censored
*/
const express = require('express')
const cookieParser = require('cookie-parser')()
const cors = require('cors')({origin: true})
const app = express()
const appCron = express()
const moment = require('moment')
const admin = require('firebase-admin')
admin.initializeApp(functions.config().firebase)

// Init admin db
var db = admin.firestore()
const usernames = db.collection('usernames')
const users = db.collection('users')
const hitchs = db.collection('hitchs')
const routes = db.collection('routes')
const deals = db.collection('deals')
const rates = db.collection('rates')
const level1 = JSON.parse('{"type":"FeatureCollection","name":"level1","crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}},"features":[{"type":"Feature","properties":{"FID":0,"sid":0},"geometry":{"type":"Polygon","coordinates":[[[19.25546847588376,102.0959641876],[19.2325187621,102.10439469470424],[19.2325187621,102.0959641876],[19.25546847588376,102.0959641876]]]}},{"type":"Feature","properties":{"FID":1,"sid":1},"geometry":{"type":"Polygon","coordinates":[[[20.43105463448391,97.93580475485217],[20.444401253879562,98.0959641876],[20.48606792054623,98.5959641876],[20.527734587212898,99.0959641876],[20.569401253879562,99.5959641876],[20.61106792054623,100.0959641876],[20.652734587212898,100.5959641876],[20.694401253879565,101.0959641876],[20.7325187621,101.55337428624523],[20.7325187621,101.55337428654097],[20.616579586994867,101.5959641876],[20.2325187621,101.73704775592873],[19.7325187621,101.92072122531648],[19.25546847588376,102.0959641876],[19.2325187621,102.0959641876],[18.7325187621,102.0959641876],[18.2325187621,102.0959641876],[17.7325187621,102.0959641876],[17.2325187621,102.0959641876],[16.7325187621,102.0959641876],[16.2325187621,102.0959641876],[15.7325187621,102.0959641876],[15.7325187621,101.5959641876],[15.7325187621,101.0959641876],[15.7325187621,100.5959641876],[15.7325187621,100.0959641876],[15.7325187621,99.5959641876],[15.7325187621,99.0959641876],[15.7325187621,98.5959641876],[15.7325187621,98.0959641876],[15.7325187621,97.5959641876],[15.7325187621,97.34282374374588],[16.2325187621,97.15785264547998],[16.399810999806817,97.0959641876],[16.7325187621,96.97288154721409],[17.2325187621,96.7879104489482],[17.7325187621,96.60293935068229],[17.751373499806817,96.5959641876],[17.751373499979586,96.5959641876],[18.2325187621,96.8365368186602],[18.7325187621,97.0865368186602],[18.751373499979582,97.0959641876],[19.2325187621,97.3365368186602],[19.7325187621,97.58653681866022],[19.75137349997958,97.5959641876],[20.2325187621,97.83653681866022],[20.43105463448391,97.93580475485217]]]}},{"type":"Feature","properties":{"FID":2,"sid":2},"geometry":{"type":"Polygon","coordinates":[[[11.7325187621,101.53315081917306],[11.885534083150569,101.0959641876],[11.889571018017193,101.08443008798108],[11.7325187621,100.96362066035246],[11.254565347521796,100.5959641876],[11.2325187621,100.57900527573709],[10.7325187621,100.1943898911217],[10.7325187621,100.0959641876],[10.7325187621,99.5959641876],[10.7325187621,99.0959641876],[10.7325187621,98.5959641876],[10.7325187621,98.18492643400432],[11.2325187621,98.4115451390403],[11.639411583525371,98.5959641876],[11.7325187621,98.63816384407627],[11.956563046381639,98.7397090952255],[12.2325187621,98.63762143160716],[12.345123499806828,98.5959641876],[12.7325187621,98.45265033334125],[13.2325187621,98.26767923507536],[13.696685999806824,98.0959641876],[13.7325187621,98.08270813680946],[14.2325187621,97.89773703854357],[14.7325187621,97.71276594027768],[15.048248499806823,97.5959641876],[15.2325187621,97.52779484201177],[15.7325187621,97.34282374374588],[15.7325187621,97.5959641876],[15.7325187621,98.0959641876],[15.7325187621,98.5959641876],[15.7325187621,99.0959641876],[15.7325187621,99.5959641876],[15.7325187621,100.0959641876],[15.7325187621,100.5959641876],[15.7325187621,101.0959641876],[15.7325187621,101.5959641876],[15.7325187621,102.0959641876],[15.2325187621,102.0959641876],[14.7325187621,102.0959641876],[14.2325187621,102.0959641876],[13.7325187621,102.0959641876],[13.2325187621,102.0959641876],[12.7325187621,102.0959641876],[12.2325187621,102.0959641876],[11.7325187621,102.0959641876],[11.53553408315057,102.0959641876],[11.710534083150568,101.5959641876],[11.7325187621,101.53315081917306]]]}},{"type":"Feature","properties":{"FID":3,"sid":3},"geometry":{"type":"Polygon","coordinates":[[[16.7325187621,105.61837057320488],[16.2325187621,105.6990157344952],[15.7325187621,105.77966089578553],[15.2325187621,105.86030605707586],[14.7325187621,105.94095121836618],[14.2325187621,106.0215963796565],[13.899331868950538,106.07533620113222],[13.907321402509409,105.5959641876],[13.915654735842741,105.0959641876],[13.923988069176076,104.5959641876],[13.932321402509409,104.0959641876],[13.932827883132761,104.06557535019887],[13.7325187621,103.83321676980087],[13.527990673995806,103.5959641876],[13.2325187621,103.25321676980087],[13.096956191237183,103.0959641876],[13.0954275285772,103.09419093891442],[12.7325187621,103.09419093891442],[12.2325187621,103.09419093891442],[11.7325187621,103.09419093891442],[11.2325187621,103.09419093891442],[11.186154720190522,103.09419093891442],[11.2325187621,102.96172224774449],[11.360534083150569,102.5959641876],[11.53553408315057,102.0959641876],[11.7325187621,102.0959641876],[12.2325187621,102.0959641876],[12.7325187621,102.0959641876],[13.2325187621,102.0959641876],[13.7325187621,102.0959641876],[14.2325187621,102.0959641876],[14.7325187621,102.0959641876],[15.2325187621,102.0959641876],[15.7325187621,102.0959641876],[16.2325187621,102.0959641876],[16.7325187621,102.0959641876],[17.2325187621,102.0959641876],[17.7325187621,102.0959641876],[18.2325187621,102.0959641876],[18.7325187621,102.0959641876],[19.2325187621,102.0959641876],[19.2325187621,102.10439469470424],[19.091214067195015,102.1563025418122],[18.950703644314373,102.5959641876],[18.790909829881386,103.0959641876],[18.7325187621,103.27867172227079],[18.631116015448395,103.5959641876],[18.471322201015404,104.0959641876],[18.311528386582417,104.5959641876],[18.2325187621,104.84318785130304],[18.151734572149426,105.0959641876],[18.05283762754612,105.40541591748777],[17.7325187621,105.45708025062424],[17.2325187621,105.53772541191456],[16.87143835285028,105.5959641876],[16.7325187621,105.61837057320488]]]}},{"type":"Feature","properties":{"FID":4,"sid":4},"geometry":{"type":"Polygon","coordinates":[[[10.2325187621,97.95830772896836],[10.53623698035077,98.0959641876],[10.7325187621,98.18492643400432],[10.7325187621,98.5959641876],[10.7325187621,99.0959641876],[10.7325187621,99.5959641876],[10.7325187621,100.0959641876],[10.7325187621,100.1943898911217],[10.60456534752179,100.0959641876],[10.583226464910519,100.07954966251441],[10.559405385822888,100.0959641876],[10.2325187621,100.32121379386284],[9.833795629725325,100.5959641876],[9.7325187621,100.66575160898888],[9.2325187621,101.01028942411493],[9.108185873627761,101.0959641876],[8.7325187621,101.35482723924098],[8.3825761175302,101.5959641876],[8.2325187621,101.69936505436704],[7.7325187621,102.04390286949308],[7.656966361432635,102.0959641876],[7.2325187621,102.38844068461913],[6.931356605335071,102.5959641876],[6.7325187621,102.73297849974517],[6.59720077722605,102.82622282545664],[6.2325187621,102.6844020417965],[6.005107137023278,102.5959641876],[5.7325187621,102.48995759735206],[5.2325187621,102.29551315290762],[4.78841601138604,102.12280652762996],[4.800691471765594,102.0959641876],[5.02935000835096,101.5959641876],[5.2325187621,101.15170184606878],[5.258008544936327,101.0959641876],[5.486667081521692,100.5959641876],[5.715325618107058,100.0959641876],[5.7325187621,100.05836851273544],[5.943984154692424,99.5959641876],[6.172642691277789,99.0959641876],[6.2325187621,98.96503517940211],[6.401301227863155,98.5959641876],[6.629959764448521,98.0959641876],[6.7325187621,97.87170184606877],[6.858618301033887,97.5959641876],[7.087276837619253,97.0959641876],[7.2325187621,96.77836851273544],[7.30061707505272,96.62946020174549],[7.7325187621,96.8252142037885],[8.2325187621,97.05183290882447],[8.32988777400157,97.0959641876],[8.7325187621,97.27845161386044],[9.2325187621,97.50507031889641],[9.433062377176169,97.5959641876],[9.7325187621,97.73168902393239],[10.2325187621,97.95830772896836]]]}}]}');

exports.aggregateRatings = functions.firestore
  .document('users/{userId}/ratings/{ratingId}')
  .onWrite(event => {
    // Get value of the newly added rating
    var ratingVal = event.data.get('rating')
    var userRef = db.collection('users').document(event.params.userId)

    // Update aggregations in a transaction
    return db.transaction(transaction => {
      return transaction.get(userRef).then(restDoc => {
        // Compute new number of ratings
        var newNumRatings = restDoc.data('numRatings') + 1

        // Compute new average rating
        var oldRatingTotal = restDoc.data('avgRating') * restDoc.data('numRatings')
        var newAvgRating = (oldRatingTotal + ratingVal) / newNumRatings

        // Update user info
        return transaction.update(userRef, {
          avgRating: newAvgRating,
          numRatings: newNumRatings
        })
      })
    })
})




// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = (req, res, next) => {
    console.log('Check if request is authorized with Firebase ID token');
  
    if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
        !req.cookies.__session) {
      console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
          'Make sure you authorize your request by providing the following HTTP header:',
          'Authorization: Bearer <Firebase ID Token>',
          'or by passing a "__session" cookie.')
      res.status(403).send('Unauthorized')
      return
    }
  
    let idToken
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
      console.log('Found "Authorization" header')
      // Read the ID Token from the Authorization header.
      idToken = req.headers.authorization.split('Bearer ')[1]
    //   res.status(200).send('authorized')      
    } else {
      console.log('Found "__session" cookie')
      // Read the ID Token from cookie.
      idToken = req.cookies.__session
    }
    admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
      // console.log('ID Token correctly decoded', decodedIdToken);
      req.user = decodedIdToken
      next()
    }).catch(error => {
      console.error('Error while verifying Firebase ID token:', error)
      res.status(403).send('Unauthorized')
    })
  }
  
  app.use(cors)
  app.use(cookieParser)
  app.use(validateFirebaseIdToken)

  // enforce uniqueness on username
  app.post('/setnewuser', (req, res) => {
    let uName = req.body.uName
    let fName = req.body.fName
    let lName = req.body.lName
    let bDate = req.body.bDate
    let publicData = {}
    let privateData = {}

    if (req.body.uName.length < 6) {
      return res.status(400).json({
        status: 400,
        message: 'username must be at least 6 characters'
      })
    }
    if (req.body.fName.data.length < 1 || req.body.lName.data.length < 1) {
      return res.status(400).json({
        status: 400,
        message: 'fisrtname and lastname must be at least 6 characters'
      })
    }
    if (!moment(req.body.bDate.data, moment.ISO_8601, true).isValid()) {
      return res.status(400).json({
        status: 400,
        message: 'birth date isn\'t valid'
      })
    }

    uName.data = req.body.uName.data.trim()
    fName.data = req.body.fName.data.trim().toLowerCase()
    lName.data = req.body.lName.data.trim().toLowerCase()
    bDate.data = moment(req.body.bDate.data, moment.ISO_8601, true).toDate()
    let userRef = users.doc(req.user.uid)
    let uNameRef = usernames.doc(uName.data)
    let uNameQuery = usernames.where('uid', '==', req.user.uid)

    db.runTransaction((tx) => {
      return tx.get(userRef).then((userDoc) => {
        if (userDoc.exists) {
          return Promise.reject({ status: 400, message: 'You are already a user.' })
        }
        return Promise.resolve()
      })
      .then(() => tx.get(uNameRef).then((uNameDoc) => {
        if (uNameDoc.exists) {
          return Promise.reject({ status: 400, message: 'username already taken' })
        }
        return Promise.resolve()
      }))
      .then(() => {
        publicData['uName'] = uName.data
        if (fName.isPublic) {publicData['fName'] = fName.data} else {privateData['fName'] = fName.data}
        if (lName.isPublic) {publicData['lName'] = lName.data} else {privateData['lName'] = lName.data}
        if (bDate.isPublic) {publicData['bDate'] = bDate.data} else {privateData['bDate'] = bDate.data}
        if (tel.isPublic) {publicData['tel'] = tel.data} else {privateData['tel'] = tel.data}
        // optional properties
        privateData['address'] = ''
        tx.set(userRef.collection('info').doc('public'), publicData)
        tx.set(userRef.collection('info').doc('private'), privateData)
      })

      // assign the username to the authenticated user
      .then(() => tx.set(uNameRef, {uid: req.user.uid}, {merge:true}))
    }).then(() => {
      res.json({
        username: uName,
        message: 'Successfully'
      })
    }).catch((err) => {
      console.log(err)
      return res.status(err.status || 500).json(err)
    })
  })

  app.post('/updateuser', (req, res) => {
    let fName = req.body.fName
    let lName = req.body.lName
    let tel = req.body.tel
    let bDate = req.body.bDate
    let address = req.body.address
    let refUsers = req.body.refUsers
    let publicData = {}
    let privateData = {}

    let userRef = users.doc(req.user.uid)
    let publicRef = userRef.collection('info').doc('public')
    let privateRef= userRef.collection('info').doc('private')
    
    db.runTransaction((tx) => {
      return Promise.all([tx.get(publicRef), tx.get(privateRef)])
      .then((userDocs) => {
        let publicDoc = userDocs[0]
        let privateDoc = userDocs[1]
        if (!(publicDoc.exists && privateDoc.exists)) {
          return Promise.reject({ status: 400, message: 'user not found.' })
        }
        let publicData = publicDoc.data()
        let privateData = privateDoc.data()
        movePublicPrivateUnchangedData('fName', fName, publicData, privateData)
        movePublicPrivateUnchangedData('lName', lName, publicData, privateData)
        movePublicPrivate('tel', tel, publicData, privateData)
        movePublicPrivateUnchangedData('bDate', bDate, publicData, privateData)
        movePublicPrivate('address', address, publicData, privateData)
        privateData['refUsers'] = refUsers.data

        tx.set(publicRef, publicData)
        tx.set(privateRef, privateData)
      })
    }).then(() => {
      res.json({
        message: 'update successfully'
      })
    }).catch((err) => {
      console.log(err)
      return res.status(err.status || 500).json(err)
    })
  })

  function movePublicPrivate (propName, input, publicData, privateData) {
    if (input.isPublic) {
      publicData[propName] = input.data
      delete privateData[propName]
    } else {
      privateData[propName] = input.data
      delete publicData[propName]
    }
  }

  function movePublicPrivateUnchangedData (propName, input, publicData, privateData) {
    if (input.isPublic) {
      publicData[propName] = publicData[propName] ? publicData[propName] : privateData[propName]
      delete privateData[propName]
    } else {
      privateData[propName] = publicData[propName] ? publicData[propName] : privateData[propName]
      delete publicData[propName]
    }
  }
//-------------------------------------------------------------------
  app.post('/findroutes', (req, res) => {
    let scopeRadius = 40; // Todo: req.body.scopeRadius
    let srcDistanceTolerance = req.body.srcDistanceTolerance ? req.body.srcDistanceTolerance/1000: 0.1;
    let destDistanceTolerance = req.body.destDistanceTolerance ? req.body.destDistanceTolerance/1000: 0.1;
    let srcCoord = [parseFloat(req.body.srcCoord.lat), parseFloat(req.body.srcCoord.lng)];
    let destCoord = [parseFloat(req.body.destCoord.lat), parseFloat(req.body.destCoord.lng)];
    let srcCoordLngLat = [srcCoord[1], srcCoord[0]]
    let destCoordLngLat = [destCoord[1], destCoord[0]]
    let sidsContainsSrcCoord = findSidByPoint(level1.features, srcCoordLngLat);
    let sidsContainsDestCoord = findSidByPoint(level1.features, destCoordLngLat);
    let mergedSids = sidsContainsSrcCoord.concat(sidsContainsDestCoord);
    let matchedRoutes = []
    let ownerDocPromises = []
    // Remove duplicates
    mergedSids = Array.from(new Set(mergedSids));

    let routesPromise = findRouteScopes(mergedSids, req.user.uid)
    .then(docs => {
      for (const key in docs) {
        let doc = docs[key];
        let decodedLine = polyline.decode(doc.encodedLine);
        let lineString = geoLib.lineString(decodedLine);
        let lineStringGeoJSON = geoLib.flip(lineString)
        let inputDirection = geoLib.bearingToAzimuth(geoLib.bearing(srcCoordLngLat, destCoordLngLat));
        let lineDirection = geoLib.bearingToAzimuth(directionOnTheLine(lineStringGeoJSON, srcCoordLngLat, destCoordLngLat));

        if (isSameDirection(inputDirection, lineDirection, scopeRadius) 
        && isInRangeArea(srcCoordLngLat, lineStringGeoJSON, srcDistanceTolerance) 
        && isInRangeArea(destCoordLngLat, lineStringGeoJSON, destDistanceTolerance)) {
            matchedRoutes.push({
              id: key,
              encodedLine: doc.encodedLine,
              title: doc.title,
              owner: doc.owner
            })
        }
      }
      return matchedRoutes
    })
    
    routesPromise
    .then((routes) => {
      Promise.all(
        routes.map((route) => {
          return db.collection("users").doc(route.owner+"/info/public").get()
          .then((user) => {
            let userData = user.data()
            return {
              id: route.id,
              encodedLine: route.encodedLine,
              points: route.points,
              title: route.title,
              owner: {
                id: route.owner,
                name: userData.uName,
                photoURL: userData.avatar
              }
            }
          })
        })
      )
      .then((routesWithUserDetail) => {
        res.json(routesWithUserDetail)
      })
    })
    .catch(function(err) {
      console.error(err)
      return res.status(err.status || 500).json(err)
    })
  })
//-------------------------------------------------------------------
  app.post('/findhitchs', (req, res) => {
    let scopeRadius = 40; // Todo: req.body.scopeRadius
    let srcDistanceTolerance = req.body.srcDistanceTolerance ? req.body.srcDistanceTolerance/1000: 0.1;
    let destDistanceTolerance = req.body.destDistanceTolerance ? req.body.destDistanceTolerance/1000: 0.1;
    let encodedLine = req.body.encodedLine
    let decodedLine = polyline.decode(encodedLine);
    let lineString = geoLib.lineString(decodedLine);    
    let lineStringGeoJSON = geoLib.flip(lineString)
    let sids = polygonsContainsLine(level1, lineStringGeoJSON);
    sids = sidObjectToArray(sids)
    let matchedHitchReqs = []

    let hitchReqPromise = findHitchReqScopes(sids, req.user.uid)
    .then(docs => {
      for (const key in docs) {
        const doc = docs[key];
        let srcCoord = doc.points.src.latLng;
        let destCoord = doc.points.dest.latLng;
        let srcCoordLngLat = [srcCoord[1], srcCoord[0]]
        let destCoordLngLat = [destCoord[1], destCoord[0]]
        doc.points.src.latLng = {
          lat: doc.points.src.latLng[0],
          lng: doc.points.src.latLng[1]
        };
        doc.points.dest.latLng = {
          lat: doc.points.dest.latLng[0],
          lng: doc.points.dest.latLng[1]
        };
        var inputDirection = geoLib.bearingToAzimuth(geoLib.bearing(srcCoordLngLat, destCoordLngLat));
        var lineDirection = geoLib.bearingToAzimuth(directionOnTheLine(lineStringGeoJSON, srcCoordLngLat, destCoordLngLat));

        if (isSameDirection(inputDirection, lineDirection, scopeRadius) 
        && isInRangeArea(srcCoordLngLat, lineStringGeoJSON, srcDistanceTolerance) 
        && isInRangeArea(destCoordLngLat, lineStringGeoJSON, destDistanceTolerance)) {
          matchedHitchReqs.push({
            id: key,
            title: doc.title,
            points: doc.points,
            owner: doc.owner
          })
        }
      }
      return matchedHitchReqs
    })
    
    hitchReqPromise.then((hitchReqs) => {
      Promise.all(
        hitchReqs.map((hitchReq) => {
          return db.collection("users").doc(hitchReq.owner+"/info/public").get()
          .then((user) => {
            let userData = user.data()
            return {
              id: hitchReq.id,
              title: hitchReq.title,
              points: hitchReq.points,
              owner: {
                id: hitchReq.owner,
                name: userData.uName,
                photoURL: userData.avatar
              }
            };
          })
        })
      )
      .then((hitchReqsWithUserDetail) => {
        res.json(hitchReqsWithUserDetail)
      })
    })
    .catch(function(err) {
      console.error(err)
      return res.status(err.status || 500).json(err)
    })
  })
//--------------------------------------------------------
  app.post('/createroute', (req, res) => {
    let reqData = req.body
    if (!moment(reqData.sTime, moment.ISO_8601, true).isValid()) {
      return res.status(500).json('sTime date isn\'t valid')
    }
    if (isNaN(reqData.price)) {
      return res.status(500).json('price must be numeric value')
    }
    reqData.price = parseFloat(reqData.price)

    let routeDocRef = db.collection('routes').doc()
    let lineDecoded = polyline.decode(reqData.encodedLine);
    let lineString = geoLib.lineString(lineDecoded)
    let lineStringGeoJSON = geoLib.flip(lineString)
    let scopeAreas = polygonsContainsLine(level1, lineStringGeoJSON);
    let dealDocRef = deals.doc()

    let txCommit = function () {
      let vehicleDocRef = users.doc(req.user.uid).collection('vehicles').doc(reqData.vehicle.id)
      return db.runTransaction((tx) => {
        let times = {
          sTime: moment(reqData.sTime, moment.ISO_8601, true).toDate()
        }
        reqData.sTime = times.sTime
        if (moment(reqData.eTime, moment.ISO_8601, true).isValid()) {
          times.eTime = moment(reqData.eTime, moment.ISO_8601, true).toDate()
          reqData.eTime = times.eTime              
        }
        if (reqData.deal) {
          let hitchDocRef = hitchs.doc(reqData.deal.hitchId)
          return Promise.all([tx.get(hitchDocRef), tx.get(vehicleDocRef)]).then((results) => {
            let hitchDoc = results[0]
            let vehicleDoc = results[1]
            if (!vehicleDoc.exists) {
              throw String('The vehicle isn\'t exist !')
            }
            if (!hitchDoc.exists) {
              throw String('The hitch isn\'t exist !')
            }
            if (hitchDoc.data().status !== 'active') {
              throw String('The hitch isn\'t available !')
            }
            let routeData = makeRouteData(reqData, req.user.uid, vehicleDoc)
            let hitchData = hitchDoc.data()
            let hitch = {id: hitchDocRef.id, data: hitchData}
            let route = {id: routeDocRef.id, data: routeData}
            
            tx = txSetDealRequest(
              tx,
              dealDocRef,
              req.user.uid,
              'd2p',
              {[req.user.uid]: 'd', [hitchData.owner]: 'p'},
              hitch,
              route,
              reqData.price,
              times,
              reqData.deal.data.message
            )
            routeData.dealWith = {}
            routeData.dealWith[hitchData.owner] = hitchDocRef
            tx.set(routeDocRef, routeData)
            routeData['scopeAreas'] = scopeAreas
            tx.set(db.collection('adminRoutes').doc(routeDocRef.id), routeData)          
          })
        } else {
          return tx.get(vehicleDocRef).then((vehicleDoc) => {
            if (!vehicleDoc.exists) {
              throw String('The vehicle isn\'t exist !')
            }
            let routeData = makeRouteData(reqData, req.user.uid, vehicleDoc)
            tx.set(routeDocRef, routeData)
            routeData['scopeAreas'] = scopeAreas
            tx.set(db.collection('adminRoutes').doc(routeDocRef.id), routeData)
          })
        }
      })
    }

    txCommit().then(function() {
      console.log("Batch committed!")
      return res.json({id: routeDocRef.id})
    }).catch(function(err) {
      console.log("Batch failed: ", err)
      return res.status(err.status || 500).json(err)
    })
  })
//--------------------------------------------------------
  app.post('/existp2d', (req, res) => {
    let reqData = req.body
    if (!reqData.hitchId) {
      return res.status(500).json('hitch is required.') 
    }
    if (!reqData.routeId) {
      return res.status(500).json('route is required.') 
    }
    if (!moment(reqData.offer.sTime, moment.ISO_8601, true).isValid()) {
      return res.status(500).json('sTime date isn\'t valid')
    }
    if (isNaN(reqData.offer.price)) {
      return res.status(500).json('price must be numeric value')
    }
    reqData.offer.price = parseFloat(reqData.offer.price)
    let dealDocRef = deals.doc()
    
    return db.runTransaction((tx) => {
      let hitchDocRef = hitchs.doc(reqData.hitchId)
      let routeDocRef = routes.doc(reqData.routeId)
      return Promise.all([tx.get(hitchDocRef), tx.get(routeDocRef)]).then((results) => {
        let hitchDoc = results[0]
        let routeDoc = results[1]

        if (!hitchDoc.exists || hitchDoc.data().status != 'active') {
          throw String('The hitch isn\'t available!')
        }
        if (!routeDoc.exists || routeDoc.data().status != 'active') {
          throw String('The route isn\'t available!')
        }
        if (hitchDoc.data().owner != req.user.uid) {
          throw String('Permission Denied!')
        }
        let hitchObj = {id: hitchDoc.id, data: hitchDoc.data()}
        hitchObj.data.points = reqData.offer.points
        let times = {
          sTime: moment(reqData.offer.sTime, moment.ISO_8601, true).toDate()
        }
        if (moment(reqData.offer.eTime, moment.ISO_8601, true).isValid()) {
          times.eTime = moment(reqData.offer.eTime, moment.ISO_8601, true).toDate()
        }
        let routeObj = {id: routeDoc.id, data: routeDoc.data()}
        tx = txSetDealRequest(
          tx,
          dealDocRef,
          req.user.uid,
          'p2d',
          {[req.user.uid]: 'p', [routeDoc.data().owner]: 'd'},
          hitchObj,
          routeObj,
          reqData.offer.price,
          times,
          reqData.offer.message
        )
        tx = txSetDealWith(tx, hitchDocRef, hitchObj, routeObj)
      })
    }).then(function() {
      console.log("Transaction successfully committed!")
      return res.json({dealId: dealDocRef.id, message: 'successfully'})
    }).catch(function(err) {
      console.log("Transaction failed: ", err)
      return res.status(err.status || 500).json(err)
    })
  })
//--------------------------------------------------------
  app.post('/createhitch', (req, res) => {
    let reqData = req.body
    if (!moment(reqData.hitch.sTime, moment.ISO_8601, true).isValid()) {
      return res.status(500).json('sTime date isn\'t valid')
    }
    if (isNaN(reqData.hitch.price)) {
      return res.status(500).json('price must be numeric value')
    }
    reqData.hitch.price = parseFloat(reqData.hitch.price)

    let hitchDocRef = hitchs.doc()
    let dealDocRef = deals.doc()

    let times = {
      sTime: moment(reqData.hitch.sTime, moment.ISO_8601, true).toDate()
    }
    if (moment(reqData.hitch.eTime, moment.ISO_8601, true).isValid()) {
      times.eTime = moment(reqData.hitch.eTime, moment.ISO_8601, true).toDate()
    }
    let hitchData = {
      title: reqData.hitch.title,
      owner: req.user.uid,
      price: reqData.hitch.price,
      points: {
        src: {
          text: reqData.src.text,
          latLng: reqData.src.latLng
        },
        dest: {
          text: reqData.dest.text,
          latLng: reqData.dest.latLng
        }
      },
      allowReq: reqData.hitch.allowReq, // boolean
      status: 'active',
      // ?matchedRef: if status matched (subRequests.docId)
      created: admin.firestore.FieldValue.serverTimestamp()
    }
    if (reqData.hitch.repeat && reqData.hitch.repeat.length > 0) {
      hitchData.repeat = reqData.hitch.repeat
    }
    hitchData.sTime = times.sTime
    if (times.eTime) {
      hitchData.eTime = times.eTime
    }
    let srcCoordLngLat = [hitchData.points.src.latLng[1], hitchData.points.src.latLng[0]]
    let destCoordLngLat = [hitchData.points.dest.latLng[1], hitchData.points.dest.latLng[0]]
    
    let sidsContainsSrcCoord = findSidByPoint(level1.features, srcCoordLngLat);
    let sidsContainsDestCoord = findSidByPoint(level1.features, destCoordLngLat);
    let mergedSids = sidsContainsSrcCoord.concat(sidsContainsDestCoord);
    let sidObject = sidArrayToObject(mergedSids)
    let hitch = {id: hitchDocRef.id, data: hitchData}
    let batch = db.batch();
    
    let txCommit = function () {
      if (reqData.deal) {
        return db.runTransaction((tx) => {
          let routeDocRef = routes.doc(reqData.deal.routeId)    
          return tx.get(routeDocRef).then((routeDoc) => {
            if (!routeDoc.exists) {
              throw String('The route isn\'t exist !')
            }
            if (routeDoc.data().status !== 'active') {
              throw String('The route isn\'t available !')
            }
            if (routeDoc.data().dealWith && routeDoc.data().dealWith[req.user.uid]) {
              throw String('The route isn\'t available !')
            }

            let route = {id: reqData.deal.routeId, data: routeDoc.data()}

            tx.set(hitchDocRef, hitchData)
            hitchData['scopeAreas'] = sidObject
            tx.set(db.collection('adminHitchs').doc(hitchDocRef.id), hitchData)
            
            tx = txSetDealRequest(
              tx,
              dealDocRef,
              req.user.uid,
              'p2d',
              {[req.user.uid]: 'p', [routeDoc.data().owner]: 'd'},
              hitch, 
              route,
              reqData.hitch.price,
              times,
              reqData.deal.data.message
            )
            tx = txSetDealWith(tx, hitchDocRef, hitch, route)
          })
        })
      } else {
        batch.set(hitchDocRef, hitchData)
        hitchData['scopeAreas'] = sidObject
        batch.set(db.collection('adminHitchs').doc(hitchDocRef.id), hitchData)
        return batch.commit()
      }
    }

    txCommit().then(function() {
      console.log("Transaction successfully committed!")
      return res.json({id: hitchDocRef.id})
    }).catch(function(err) {
      console.log("Transaction failed: ", err)
      return res.status(err.status || 500).json(err)
    })
  })
//--------------------------------------------------------
  app.post('/existd2p', (req, res) => {
    let reqData = req.body
    if (!reqData.hitchId) {
      return res.status(500).json('hitch is required.') 
    }
    if (!reqData.routeId) {
      return res.status(500).json('route is required.') 
    }
    if (!moment(reqData.offer.sTime, moment.ISO_8601, true).isValid()) {
      return res.status(500).json('sTime date isn\'t valid')
    }
    if (isNaN(reqData.offer.price)) {
      return res.status(500).json('price must be numeric value')
    }
    reqData.offer.price = parseFloat(reqData.offer.price)
    let dealDocRef = deals.doc()
    
    return db.runTransaction((tx) => {
      let hitchDocRef = hitchs.doc(reqData.hitchId)
      let routeDocRef = routes.doc(reqData.routeId)
      return Promise.all([tx.get(hitchDocRef), tx.get(routeDocRef)]).then((results) => {
        let hitchDoc = results[0]
        let routeDoc = results[1]

        if (!hitchDoc.exists || hitchDoc.data().status != 'active') {
          throw String('The hitch isn\'t available!')
        }
        if (!routeDoc.exists || routeDoc.data().status != 'active') {
          throw String('The route isn\'t available!')
        }
        if (routeDoc.data().owner != req.user.uid) {
          throw String('Permission Denied!')
        }
        let hitchObj = {id: hitchDoc.id, data: hitchDoc.data()}
        let times = {
          sTime: moment(reqData.offer.sTime, moment.ISO_8601, true).toDate()
        }
        if (moment(reqData.offer.eTime, moment.ISO_8601, true).isValid()) {
          times.eTime = moment(reqData.offer.eTime, moment.ISO_8601, true).toDate()
        }
        let routeObj = {id: routeDoc.id, data: routeDoc.data()}
         tx = txSetDealRequest(
          tx,
          dealDocRef,
          req.user.uid,
          'd2p',
          {[req.user.uid]: 'd', [hitchDoc.data().owner]: 'p'},
          hitchObj,
          routeObj,
          reqData.offer.price,
          times,
          reqData.offer.message
        )
        tx = txSetDealWith (tx, hitchDocRef, hitchObj, routeObj)
      })
    }).then(function() {
      console.log("Transaction successfully committed!")
      return res.json({dealId: dealDocRef.id, message: 'successfully'})
    }).catch(function(err) {
      console.log("Transaction failed: ", err)
      return res.status(err.status || 500).json(err)
    })
  })
//--------------------------------------------------------
  app.post('/acceptdeal', (req, res) => {
    let hitchId = req.body.hitchId
    let routeId = req.body.routeId
    let dealId = req.body.dealId
    let subDealId = req.body.subDealId
    if (!dealId || !subDealId) {
      return res.status(500).json({message: 'check your inputs'})
    }
    let hitchDocRef = hitchs.doc(hitchId)
    let routeDocRef = routes.doc(routeId)
    let dealDocRef = deals.doc(dealId)
    let subDealDocRef = dealDocRef.collection('subDeals').doc(subDealId)

    return db.runTransaction((tx) => {
      return Promise.all([tx.get(hitchDocRef), tx.get(routeDocRef), tx.get(dealDocRef), tx.get(subDealDocRef)]).then((results) => {
        let hitch = results[0]
        let route = results[1]
        let deal = results[2]
        let subDeal = results[3]
        if (!hitch.exists || !route.exists || !deal.exists || !subDeal.exists) {
          throw String('The deal doesn\'t exists!')
        }
        // only deal member && user can't accept own deal.
        if (!deal.data().users[req.user.uid] || subDeal.data().by === req.user.uid) {
          throw String('Permission Denied!')
        }
        if (deal.data().refs.hitchId !== hitch.id || deal.data().refs.routeId !== route.id) {
          throw String('Inputs aren\'t correct!')
        }
        if (deal.data().status !== 'active') {
          throw String('Can\'t set the status!')
        }
        if (route.data().seatLeft < 1) {
          throw String('No seat available!')
        }
        // keep some orginal data because don't need to keep data for matching anymore.
        tx.update(db.collection('adminHitchs').doc(hitchDocRef.id), {
          status: 'matched'
        })        
        tx.set(hitchDocRef, {
          title: hitch.data().title,
          owner: hitch.data().owner,
          points: hitch.data().points,
          created: hitch.data().created,
          status: 'matched',
          matchRef: subDealDocRef
        })
        tx.update(routeDocRef, {
          seatLeft: route.data().seatLeft - 1
        })
        tx.update(dealDocRef, {
          status: 'matched',
          matchRefId: subDealDocRef.id
        })
      })
    }).then(function() {
      console.log("Transaction successfully committed!")
      return res.json({message: 'successfully'})      
    }).catch(function(err) {
      console.log("Transaction failed: ", err)
      return res.status(err.status || 500).json(err)
    })
  })
//--------------------------------------------------------
  app.post('/completedeal', (req, res) => {
    let dealId = req.body.dealId
    if (!dealId) {
      return res.status(500).json({message: 'check your inputs'})
    }

    let dealDocRef = deals.doc(dealId)
    return db.runTransaction((tx) => {
      return Promise.all([tx.get(dealDocRef)]).then((results) => {
        let dealDoc = results[0]
        if (!dealDoc.exists) {
          throw String('The deal doesn\'t exists!')
        }
        let dealData = dealDoc.data()
        if (dealData.status !== 'matched') {
          throw String('Can\'t set the status!')
        }
        // Only passenger can complete the deal. then money transfer to rider.
        if (dealData.users[req.user.uid] !== 'p') {
          throw String('Permission Denied!')
        }
        tx.set(dealDocRef, {
          status: 'done'
        }, {merge: true})
        tx.set(rates.doc(), {
          status: 'wait',
          owner: req.user.uid,
          to: getKeyByValue(dealData.users, 'd'),
          dealRef: dealDocRef
        })
        tx.set(rates.doc(), {
          status: 'wait',
          owner: getKeyByValue(dealData.users, 'd'),
          to: getKeyByValue(dealData.users, 'p'),
          dealRef: dealDocRef
        })
      })
    }).then(function() {
      console.log("Transaction successfully committed!")
      return res.json({message: 'successfully'})      
    }).catch(function(err) {
      console.log("Transaction failed: ", err)
      return res.status(500).json(err)
    })
  })
//--------------------------------------------------------
  appCron.get('/rateaggregation', (req, res) => {
    let ratedUsers = []
    return db.runTransaction((tx) => {
      return tx.get(rates.where('status', '==', 'rated')).then(ratingDocs => {
        ratingDocs.forEach(ratingDoc => {
          let ratingData = ratingDoc.data()
          let toUser = ratingData.to
          ratedUsers.push(toUser)
          ratingData.created = admin.firestore.FieldValue.serverTimestamp()
          // delete no longer needed properties.
          delete ratingData.to
          delete ratingData.status

          tx.set(users.doc(toUser).collection('rates').doc(ratingDoc.ref.id), ratingData)
        })
      })
    }).then(() => {
      // remove duplicate
      ratedUsers = Array.from(new Set(ratedUsers))
      for (let i = 0; i < ratedUsers.length; i++) {
        let aggregateVal = 0
        users.doc(ratedUsers[i]).collection('rates').orderBy('created', 'desc').limit(100).get().then(ratings => {
          for (let j = 0; j < ratings.docs.length; j++) {
            aggregateVal += ratings.docs[j].data().val
          }
          users.doc(ratedUsers[i]).collection('info').doc('public').set({
            ratings: aggregateVal/ratings.docs.length
          }, {merge: true})
        })
      }
      res.status(200).send('complete')
    }).catch(function(err) {
      console.log("Rating transaction failed: ", err)
      return res.status(500).json(err)
    })
  })
//--------------------------------------------------------
function polygonsContainsLine(polygons, line) {
  /*
  ...
  ...
  censored
  */
  // return matched areas
  return matchedSids;
}

function makeRouteData(reqData, owner, vehicleDoc) {
  let vehicleData = vehicleDoc.data()
  let data = {
    title: reqData.title,
    encodedLine: reqData.encodedLine,
    stops: reqData.stops,
    // for checking when create a hitchhike, Can't get ownerId in roleObject
    // and it isn't worth to put ownerId of route with hitch.(take more storage)
    owner: owner,
    price: reqData.price,
    status: 'active',
    sTime: reqData.sTime,
    vehicle: {
      id: vehicleDoc.id,
      color: vehicleData.color,
      desc: vehicleData.desc,
      img: vehicleData.img,
      name: vehicleData.name,
      seatLeft: reqData.vehicle.seatLeft
    },
    created: admin.firestore.FieldValue.serverTimestamp()
  }
  if (reqData.repeat && reqData.repeat.length > 0) {
    data.repeat = reqData.repeat
  }
  if (reqData.eTime) {
    data.eTime = reqData.eTime
  }
  return data
}

function sidArrayToObject(sids) {
  sidObject = {}
  sids.forEach(sid => {
    sidObject[''+sid] = true;
  });
  return sidObject
}

function sidObjectToArray(sids) {
  sidArray = []
  for (const sid in sids) {
    sidArray.push(sid)
  }
  return sidArray
}

function txSetDealRequest(tx, dealDocRef, reqUser, type, users, hitch, route, price, times, message) {
  let subDealDocRef = dealDocRef.collection('subDeals').doc()
  let threadDocRef = db.collection("threads").doc()
  
  tx.set(dealDocRef, {
    type: type,
    status: 'active',
    refs: {
      hitchId: hitch.id,
      routeId: route.id
    },
    users: users
  })
  
  let subDealData = {
    by: reqUser,
    encodedLine: route.data.encodedLine,
    msg: message,
    price: price,
    points: hitch.data.points,
    created: admin.firestore.FieldValue.serverTimestamp()
  }
  subDealData.sTime = times.sTime
  if (times.eTime) {
    subDealData.eTime = times.eTime
  }
  tx.set(subDealDocRef, subDealData)
  
  let userTrue = {}
  for (let prop in users) {
    userTrue[prop] = true
  }
  tx.set(threadDocRef, {
    name: "deal",
    status: "active",
    refData: {
      type,
      dealId: dealDocRef.id
    },
    users: userTrue
  })
  tx.set(threadDocRef.collection("messages").doc(), {
    from: reqUser,
    msg: message,
    created: admin.firestore.FieldValue.serverTimestamp()
  })
}

function txSetDealWith (tx, hitchDocRef, hitch, route) {
  route.data.dealWith = route.data.dealWith ? route.data.dealWith : {}
  route.data.dealWith[hitch.data.owner] = hitchDocRef
  tx.set(db.collection('routes').doc(route.id), {
    dealWith: route.data.dealWith
  }, {merge:true})
  tx.set(db.collection('adminRoutes').doc(route.id), {
    dealWith: route.data.dealWith
  }, {merge:true})
}

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

// find routes are in areas.
function findRouteScopes(sids, uid) {
  var routesRef = db.collection("adminRoutes");
  var docs = {};

  return Promise.all(sids.map((sid) => {
    return routesRef.where("scopeAreas."+sid, "==", true).get()
  }))
  .then((results) => {
    results.map((result) => {
      result.forEach((doc) => { 
        let docData = doc.data()
        // not owner and not made deal already
        if (docData.owner !== uid && docData.status === 'active' && !(docData.dealWith && docData.dealWith[uid])) {
          docs[doc.id] = docData
        }
      })
    })
    return docs
  }).catch(reason => {
    console.log(reason)
  });
}

// find hitchhikes are in areas.
function findHitchReqScopes(sids, uid) {
  var routesRef = db.collection("adminHitchs");
  var docs = {};

  return Promise.all(sids.map((sid) => {
    return routesRef.where("scopeAreas."+sid, "==", true).where("status", "==", "active").get()
  }))
  .then((results) => {
    results.map((result) => {
      result.forEach((doc) => {
        let docData = doc.data()
        if (docData.owner !== uid && docData.status === 'active') {
          docs[doc.id] = docData
        }
      })
    })
    return docs
  }).catch(err => {
    console.log(err)
  });
}

// find the areaId if the given point is inside.
function findSidByPoint(polygons, point) {
  let sids = [];
  for (let i = 0; i < polygons.length; i++) {
    let polygonsGeoJSON = geoLib.flip(polygons[i])
    if (geoLib.isPointInPolygon(point, polygonsGeoJSON)) {
      sids.push(polygons[i].properties.sid);
    }
  }

  return sids;
}

function isInRangeArea(point, line, tolerance, options={units: 'kilometers'}) {
  let distance = geoLib.pointToLineDistance(point, line, options);
  if (distance <= tolerance) {
      return true;
  }
  return false;
}

// Are hitchhike and route moving in same direction.
function isSameDirection(firstDirection, secondDirection, scopeRadius=30) {
  let lScope = /*censored*/;
  let rScope = /*censored*/;

  if (rScope < scopeRadius) {
      if (secondDirection <= rScope || secondDirection >= lScope)
          return true;
  } else {
      if (secondDirection >= lScope && secondDirection <= rScope)
          return true;
  }
  return false
}

// direction on the line
function directionOnTheLine(line, sourcePoint, destinyPoint) {
  /* 
  ...
  ...
  censored
  */
  return direction
}

function degreeToCompassCode(degrees) {
  if (degrees < 22.5 || degrees > 337.5)
    return "n"
  else if (degrees < 67.5)
    return "ne"
  else if (degrees < 112.5)
    return "e"
  else if (degrees < 157.5)
    return "se"
  else if (degrees < 202.5)
    return "s"
  else if (degrees < 247.5)
    return "sw"
  else if (degrees < 292.5)
    return "w"
  else //(degrees < 337.5)
    return "nw"
}

// nearest hitchhike point on the route.
function nearestPointOnLine(lines, pt, options) {
  /*
  ...
  ...
  censored.
  */
  return closestPt
}

  exports.app = functions.https.onRequest(app);

  exports.appCron = functions.https.onRequest(appCron);