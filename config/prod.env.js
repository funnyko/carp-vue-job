module.exports = {
  NODE_ENV: '"production"',
  FIREBASE: {
    apiKey: '"/*censored*/"',
    authDomain: '"/*censored*/"',
    databaseURL: '"/*censored*/"',
    projectId: '"/*censored*/"',
    storageBucket: '"/*censored*/"',
    messagingSenderId: '"/*censored*/"'
  }
}
