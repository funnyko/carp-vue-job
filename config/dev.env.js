'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"/*censored*/"',
  FIREBASE: {
    apiKey: '"/*censored*/"',
    authDomain: '"/*censored*/"',
    databaseURL: '"/*censored*/"',
    projectId: '"/*censored*/"',
    storageBucket: '"/*censored*/"',
    messagingSenderId: '"/*censored*/"'
  },
  NODE_API: '"/*censored*/"'
})
