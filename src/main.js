// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import { store } from './store'
import * as VueGoogleMaps from '../node_modules/vue2-google-maps/src/main'
import * as firebase from 'firebase'
import 'firebase/firestore'

import('vuetify/dist/vuetify.min.css')
import('firebaseui/dist/firebaseui.css')

Vue.config.productionTip = false

Vue.use(Vuetify)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'censored',
    libraries: 'places'
  }
})

const unsubscribe = firebase.initializeApp(process.env.FIREBASE)
  .auth().onAuthStateChanged(async (user) => {
    store.commit('setFirebase', firebase)
    store.commit('setFirestore', firebase.firestore())
    if (user) {
      await store.dispatch('users/autoSignIn', { user })
    }
    new Vue({
      el: '#app',
      router,
      store,
      template: '<App/>',
      components: { App }
    })
    // remove this listener so that we aren't trying to make new vue objects
    // every time the auth state changes.
    unsubscribe()
  })