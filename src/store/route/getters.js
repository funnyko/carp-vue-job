export default {
  matchedRoutes (state) {
    return state.matchedRoutes
  },
  showMatchedRoutesNav (state) {
    return state.showMatchedRoutesNav
  }
}
