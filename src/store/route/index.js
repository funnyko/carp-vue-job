import actions from './actions'
import mutations from './mutations'
import getters from './getters'

const state = {
  all: {},
  matchedRoutes: {},
  matchedHitchs: {},
  showMatchedRoutesNav: false,
  showMatchedHitchsNav: false
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  plugins: []
}
