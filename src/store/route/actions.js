import * as firebase from 'firebase'
import moment from 'moment'
import * as types from './mutation-types'
import axios from 'axios'

export default {
  async getAll ({ commit, rootState }) {
    let routesCol = rootState.firestore.collection('routes')
      .where('owner', '==', rootState.users.currentUser.uid)
    let routes = await routesCol.get()
    routes.forEach(route => commit(types.SET_ROUTE, route))
  },
  async get ({ commit, state, rootState }, routeId) {
    if (!state.all[routeId]) {
      let route = await getRouteByRouteId({ commit, state, rootState }, { routeId })
      if (!route.err) {
        commit(types.SET_ROUTE, route)
      }
    }
    return state.all[routeId] ? state.all[routeId] : { err: 'route not found' }
  },
  create ({ commit, rootState }, payload) {
    console.log('payload.deal', payload.deal)
    let url = process.env.NODE_API + 'createroute'
    let sTime = toDateTime(payload.route.sDate, payload.route.sTime)
    let eTime = toDateTime(payload.route.eDate, payload.route.eTime)
    let stops = []
    payload.route.price = (payload.route.price == '') ? 0 : payload.route.price
    for (let i = 0; i < payload.stops.length; i++) {
      let item = payload.stops[i]
      stops.push({
        text: item.text,
        latLng: [item.latLng.lat, item.latLng.lng]
      })
    }
    let data = {
      title: payload.route.title,
      /*censored*/
      sTime,
      eTime,
      vehicle: { id: payload.route.vehicle.id, name: payload.route.vehicle.name, seatLeft: payload.route.vehicle.seatLeft, color: payload.route.vehicle.color, desc: payload.route.vehicle.desc, img: payload.route.vehicle.img },
      created: firebase.firestore.FieldValue.serverTimestamp()
    }
    // if withDeal
    if (payload.deal) {
      data['deal'] = payload.deal
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, data, { headers: headers })
        .catch(err => {
          console.log('err', err.response)
          return Promise.reject(err.response)
        })
      })
  },
  async findRoutes ({commit, rootState}, payload) {
    let url = process.env.NODE_API + 'findroutes'
    const body = {
      /*censored*/
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
      })
      .then(res => { return res.data })
      .then(docs => {
        docs.map(doc => {
          if (doc.owner && !doc.owner.photoURL) doc.owner.photoURL = 'https://firebasestorage.googleapis.com/v0/b/test-confirmemail.appspot.com/o/images%2Femp_avatar.png?alt=media&token=731b6427-4143-426e-9870-30e0607233ef'          
          return {
            /*censored*/
          }
        })
        commit(types.SET_MATCHED_ROUTES, docs)
        if (docs.length > 0) {
          commit(types.SET_SHOW_MATCHED_ROUTES_NAV, true)
        }
        return docs
      })
      .catch(err => { return Promise.reject(err.response) })
  },
  async findHitchs ({commit, rootState}, payload) {
    let url = process.env.NODE_API + 'findhitchs'
    const body = {
      encodedLine: payload.encodedLine,
      srcDistanceTolerance: payload.findOptions.startMaxDistance,
      destDistanceTolerance: payload.findOptions.destMaxDistance
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
      })
      .then(res => { return res.data })
      .then(docs => {
        docs.map(doc => {
          if (doc.owner && !doc.owner.photoURL) doc.owner.photoURL = 'https://firebasestorage.googleapis.com/v0/b/test-confirmemail.appspot.com/o/images%2Femp_avatar.png?alt=media&token=731b6427-4143-426e-9870-30e0607233ef'
          return {
            /*censored*/
          }
        })

        return docs
      })
      .catch(err => { return Promise.reject(err.response) })
  },
  createExistD2p ({commit, rootState}, payload) {
    let url = process.env.NODE_API + 'existd2p'
    let sTime = toDateTime(payload.deal.sDate, payload.deal.sTime)
    let eTime = toDateTime(payload.deal.eDate, payload.deal.eTime)
    console.log('payload', payload)
    let body = {
      /*censored*/
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
      })
  }
}

async function getRouteByRouteId ({ commit, state, rootState }, { routeId }) {
  return rootState.firestore.collection('routes').doc(routeId).get().then((doc) => {
    if (doc.exists) {
      return doc
    } else {
      return { err: 'deal doesn\'t exist.' }
    }
  }).catch(err => { throw (err) })
}
