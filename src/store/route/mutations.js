import Vue from 'vue'
import * as types from './mutation-types'
import moment from 'moment'

export default {
  [types.SET_ROUTE] (state, route) {
    // create new route if the route doesn't exist
    if (!state.all[route.id]) {
      let data = route.data()
      if (data.status === 'active') {
        // The behavior for Date objects stored in Firestore is going to change
        data.sTime = (typeof data.sTime != 'undefined') ? data.sTime.toDate() : ''
        data.eTime = (typeof data.eTime != 'undefined') ? data.eTime.toDate() : ''
        data.created = data.created.toDate()
        if (moment.isDate(data.sTime)) {
          data.sDate = moment(data.sTime).format('YYYY-MM-DD')
          data.sTime = moment(data.sTime).format('HH:mm')
        }
        if (moment.isDate(data.eTime)) {
          data.eDate = moment(data.eTime).format('YYYY-MM-DD')
          data.eTime = moment(data.eTime).format('HH:mm')
        }
        for (let i = 0; i < data.stops.length; i++) {
          data.stops[i].latLng = {
            lat: data.stops[i].latLng[0],
            lng: data.stops[i].latLng[1]
          }
        }
      }
      createRoute(state, {
        id: route.id,
        ...data
      })
    }
  },
  [types.SET_LATEST_DEAL] (state, payload) {
    if (!state.all[payload.routeId]) {
      createRoute(state, {
        id: payload.routeId,
        encodedLine: null,
        status: 'virtual'
      })
    }
    state.all[payload.routeId]['deal'] = payload.deal
    console.log('SET_DEAL', state.all[payload.routeId])
  },
  [types.SET_MATCHED_ROUTES] (state, payload) {
    state.matchedRoutes = payload
  },
  [types.SET_SHOW_MATCHED_ROUTES_NAV] (state, payload) {
    state.showMatchedRoutesNav = payload
  }
}

function createRoute (state, route) {
  Vue.set(state.all, route.id, {
    id: route.id,
    type: 'route',
    link: { name: 'routeInfo', params: { routeId: route.id } },
    ...route
  })
}
