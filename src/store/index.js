import Vue from 'vue'
import Vuex from 'vuex'

import users from './user/index.js'
import threads from './thread/index.js'
import routes from './route/index.js'
import hitchs from './hitch/index.js'
import deals from './deal/index.js'
import rates from './rate/index.js'
import maps from './googlemaps/index.js'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    asyncPages: {
      showp2d: null,
      editp2d: null
    },
    finder: {
      srcMarker: null,
      destMarker: null
    },
    tempPageData: {
      existHitchs: {},
      existRoutes: {}
    },
    loading: false,
    firestore: null,
    mapsComponent: null,
    btmComponent: null
  },
  modules: {
    users,
    threads,
    routes,
    hitchs,
    deals,
    rates,
    maps
  },
  mutations: {
    setUser (state, payload) {
      if (payload) {
        state.user = {
          uid: payload.uid,
          name: payload.displayName,
          email: payload.email,
          photo: payload.photoURL
        }
      } else {
        state.user = null
      }
    },
    setFinderSrc (state, payload) {
      state.finder.srcMarker = payload
    },
    setFinderDest (state, payload) {
      state.finder.destMarker = payload
    },
    setMatchedRoute (state, payload) {
      state.matchedRoute = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setFirebase (state, payload) {
      state.firebase = payload
    },
    setFirestore (state, payload) {
      state.firestore = payload
      state.firestore.settings({ timestampsInSnapshots: true })
    },
    setMapsComponent (state, payload) {
      state.mapsComponent = payload
    },
    setBtmComponent (state, payload) {
      state.btmComponent = payload
    },
    clearError (state) {
      state.error = null
    }
  },
  actions: {
    signUserUp ({commit, state}, payload) {
      commit('setLoading', true)
      commit('clearError')
      state.firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              uid: user.uid,
              name: user.displayName,
              email: user.email,
              photo: user.photoURL
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
          }
        )
    },
    signUserIn ({commit, state}, payload) {
      commit('setLoading', true)
      commit('clearError')
      state.firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              uid: user.uid,
              name: user.displayName,
              email: user.email,
              photo: user.photoURL
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
          }
        )
    },
    logout ({commit, state}) {
      state.firebase.auth().signOut()
      commit('setUser', null)
    },
    clearError ({commit}) {
      commit('clearError')
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    finder (state) {
      return state.finder
    },
    loading (state) {
      return state.loading
    },
    error (state) {
      return state.error
    },
    firebase (state) {
      return state.firebase
    }
  }
})
