import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  all: {},
  fetchingState: '',
  p: { subScribe: null, listening: false },
  d: { subScribe: null, listening: false }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
