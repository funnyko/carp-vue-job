export default {
  isSubcribed: (state) => {
    if (state.p.subScribe && state.d.subScribe) {
      return true
    } else {
      return false
    }
  },
  dealsByHitchId: (state) => (id) => {
    let items = []
    for (let key in state.all) {
      let item = state.all[key]
      if (item.refs.hitchId === id) {
        items.push(item)
      }
    }
    return items
  },
  dealsByRouteId: (state) => (id) => {
    let items = []
    for (let key in state.all) {
      let item = state.all[key]
      if (item.refs.routeId === id) {
        items.push(item)
      }
    }
    return items
  }
}
