/*
  getAll and listen at /home
  get when specific page and state is null eg /hitch/:hitchId, /route/:routeId
*/
// import * as firebase from 'firebase'
import * as types from './mutation-types'
import axios from 'axios'
import * as firebase from 'firebase'
import moment from 'moment'

export default {
  async get ({ commit, state, rootState, dispatch }, { dealId }) {
    if (state.fetchingState !== 'listening') {
      getAll({ commit, state, rootState, dispatch })
      await waitFetchingState(state, 'listening')
    }
    return state.all[dealId]
  },
  getAll ({ commit, state, rootState, dispatch, getters }) {
    if (!getters.isSubcribed) {
      getAll({ commit, state, rootState, dispatch })
    }
  },
  create ({commit, rootState}, payload) {
    let src = { text: payload.deal.points.src.text, latLng: [payload.deal.points.src.latLng.lat, payload.deal.points.src.latLng.lng] }
    let dest = { text: payload.deal.points.dest.text, latLng: [payload.deal.points.dest.latLng.lat, payload.deal.points.dest.latLng.lng] }
    let sTime = toDateTime(payload.deal.sDate, payload.deal.sTime)
    let eTime = toDateTime(payload.deal.eDate, payload.deal.eTime)
    return rootState.firestore.doc('deals/' + payload.deal.parentDealId).collection('subDeals').add({
      /*censored*/
    })
  },
  acceptDeal ({commit, rootState}, payload) {
    console.log('payload', payload)
    let url = process.env.NODE_API + 'acceptdeal'
    let body = {
      /*censored*/
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
        .catch(err => {
          console.log('err', err.response)
          return Promise.reject(err.response)
        })
      })
  },
  completeDeal ({commit, rootState}, payload) {
    let url = process.env.NODE_API + 'completedeal'
    let body = {
      /*censored*/
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
        .catch(err => {
          console.log('err', err.response)
          return Promise.reject(err.response)
        })
      })
  }
}

async function waitFetchingState (state, checker) {
  if (state.fetchingState === checker) {
    return true
  } else {
    await new Promise(resolve => setTimeout(resolve, 500))
    let waitState = await waitFetchingState(state, checker)
    return waitState
  }
}

function getAll ({ commit, state, rootState, dispatch }) {
  if (!state.p.subScribe || !state.d.subScribe) {
    if (!state.p.state) {
      state.p.subScribe = subscribesByRole({ commit, state, rootState, dispatch }, state.p, 'p')
    }
    if (!state.d.state) {
      state.d.subScribe = subscribesByRole({ commit, state, rootState, dispatch }, state.d, 'd')
    }
  }
}

async function subscribesByRole ({ commit, state, rootState, dispatch }, caller, role) {
  return rootState.firestore.collection('deals').where('users.' + rootState.users.currentUser.uid, '==', role)
    .onSnapshot(async (snapshot) => {
      commit(types.UPDATE_FETCHING_STATE, { ref: caller, fetchingState: 'waiting' })
      await Promise.all(snapshot.docs.map(async (doc) => {
        let docData = doc.data()
        let driver = await dispatch('users/get', { userId: getKeyByValue(docData.users, 'd') }, { root: true })
        let passenger = await dispatch('users/get', { userId: getKeyByValue(docData.users, 'p') }, { root: true })
        commit(types.SET_DEAL, { deal: doc, driver, passenger })
      }))
      commit(types.UPDATE_FETCHING_STATE, { ref: caller, fetchingState: 'listening' })
    })
}