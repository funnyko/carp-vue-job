import Vue from 'vue'
import * as types from './mutation-types'
import moment from 'moment'

export default {
  [types.SET_DEAL] (state, { deal, driver, passenger }) {
    console.log('setdeal', deal)
    let data = deal.data()
    // The behavior for Date objects stored in Firestore is going to change
    data.sTime = (typeof data.sTime != 'undefined') ? data.sTime.toDate() : ''
    data.eTime = (typeof data.eTime != 'undefined') ? data.eTime.toDate() : ''
    if (moment.isDate(data.sTime)) {
      data.sDate = moment(data.sTime).format('YYYY-MM-DD')
      data.sTime = moment(data.sTime).format('HH:mm')
    }
    if (moment.isDate(data.eTime)) {
      data.eDate = moment(data.eTime).format('YYYY-MM-DD')
      data.eTime = moment(data.eTime).format('HH:mm')
    }

    Vue.set(state.all, deal.id, {
      id: deal.id,
      ...data,
      driver,
      passenger
    })
  },
  [types.UPDATE_FETCHING_STATE] (state, { ref, fetchingState }) {
    ref.state = fetchingState
    console.log('state', state.p.state, state.d.state)
    if (state.p.state === 'listening' && state.d.state === 'listening') {
      state.fetchingState = 'listening'
    } else {
      if (state.p.state === 'listening') {
        state.fetchingState = state.d.state
      } else {
        state.fetchingState = state.p.state
      }
    }
  }
}
