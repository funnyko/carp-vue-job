import Vue from 'vue'
import * as types from './mutation-types'

export default {
  [types.SET_RATING] (state, rate) {
    // create new rate if the rate doesn't exist
    if (!state.all[rate.id]) {
      createRating(state, {
        id: rate.id,
        ...rate.data()
      })
    }
  }
}

function createRating (state, rate) {
  Vue.set(state.all, rate.id, {
    ...rate
  })
}
