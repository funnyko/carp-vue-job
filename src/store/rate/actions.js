import * as types from './mutation-types'

export default {
  async getAll ({ commit, rootState }) {
    let ratesCol = rootState.firestore.collection('rates')
      .where('owner', '==', rootState.users.currentUser.uid)
      .where('status', '==', 'wait')
    let rates = await ratesCol.get()
    rates.forEach(rate => commit(types.SET_RATING, rate))
  },
  async get ({ commit, state, rootState }, rateId) {
    if (state.all[rateId]) {
      return state.all[rateId]
    } else {
      let rate = await getRateByRateId({ commit, state, rootState }, rateId)
      if (!rate.err) {
        commit(types.SET_RATING, rate)
      }
      return state.all[rateId]
    }
  },
  updateRating ({commit, rootState}, payload) {
    rootState.firestore.collection('rates').doc(payload.rateId).update({
      /*censored*/
    }).then(() => {
      return true
    }).catch(error => {
      return { err: error }
    })
  }
}
async function getRateByRateId ({ commit, state, rootState }, rateId) {
  return rootState.firestore.collection('rates').doc(rateId).get()
    .then(doc => {
      if (doc.exists) {
        return doc
      } else {
        return { err: 'rate doesn\'t exist.' }
      }
    }).catch(function (error) {
      return { err: error }
    })
}
