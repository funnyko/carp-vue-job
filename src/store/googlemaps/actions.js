export default {
  async geocodeByPlaceId  ({ commit, rootState }, options) {
    const geocoder = new google.maps.Geocoder()
    return new Promise((resolve, reject) => {
      geocoder.geocode(options, (results, status) => {
        if (status !== google.maps.GeocoderStatus.OK) {
          reject(status)
        }
        resolve(results)
      })
    })
  }
}
