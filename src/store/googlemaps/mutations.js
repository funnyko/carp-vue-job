export default {
  setMapsPlaceService (state, {maps}) {
    state.placeService = new google.maps.places.PlacesService(maps)
  },
  setMapsAutocompleteService (state) {
    state.autocompleteService = new google.maps.places.AutocompleteService()
  }
}
