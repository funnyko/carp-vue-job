import Vue from 'vue'
import * as types from './mutation-types'
import moment from 'moment'

export default {
  [types.SET_USER] (state, user) {
    // create new user if the user doesn't exist
    if (!state.all[user.id]) {
      createUser(state, {
        id: user.id,
        ...user.data
      })
    }
  },
  [types.SET_VEHICLE_CURRENT_USER] (state, vehicles) {
    state.currentUser.vehicles = vehicles
  },
  [types.SET_CURRENT_USER] (state, info) {
    if (info.publicInfo) {
      for (let key in info.publicInfo) {
        if (key == 'bDate') info.publicInfo[key] = info.publicInfo[key].toDate()
        if (moment.isDate(info.publicInfo[key])) { info.publicInfo[key] = moment(info.publicInfo[key]).format('YYYY-MM-DD') }
        state.currentUser[key] = {data: info.publicInfo[key], isPublic: true}
      }
    }
    if (info.privateInfo) {
      for (let key in info.privateInfo) {
        if (key == 'bDate') info.publicInfo[key] = info.publicInfo[key].toDate()
        if (moment.isDate(info.privateInfo[key])) { info.privateInfo[key] = moment(info.privateInfo[key]).format('YYYY-MM-DD') }
        state.currentUser[key] = {data: info.privateInfo[key], isPublic: false}
      }
    }
    console.log('info', info)    
    console.log('state.currentUser', state.currentUser)
  }
}

function createUser (state, user) {
  Vue.set(state.all, user.id, {
    ...user
  })
}
