import * as types from './mutation-types'
import axios from 'axios'
import moment from 'moment'

export default {
  async get ({ commit, state, rootState }, { userId }) {
    if (state.all[userId]) {
      return state.all[userId]
    } else {
      let userData = await getPublicInfo(rootState.firestore.collection('users').doc(userId))
      if (userData) {
        commit(types.SET_USER, { id: userId, data: userData })
      }
      return state.all[userId]
    }
  },
  async autoSignIn ({ commit, state, rootState, dispatch }, { user }) {
    let userRef = rootState.firestore.collection('users').doc(user.uid)
    state.currentUser.uid = user.uid
    return Promise.all([getPublicInfo(userRef), getPrivateInfo(userRef), getVehicles(userRef)]).then(results => {
      if (results[0] && !results[0].avatar) results[0].avatar = 'https://firebasestorage.googleapis.com/v0/b/test-confirmemail.appspot.com/o/images%2Femp_avatar.png?alt=media&token=731b6427-4143-426e-9870-30e0607233ef'
      commit(types.SET_CURRENT_USER, { publicInfo: results[0], privateInfo: results[1], vehicles: results[2] })
      commit(types.SET_VEHICLE_CURRENT_USER, results[2])
      dispatch('hitchs/getAll', null, { root: true })
      dispatch('routes/getAll', null, { root: true })
      dispatch('rates/getAll', null, { root: true })
    })
  },
  async refreshVehicles ({ commit, rootState }) {
    let userRef = rootState.firestore.collection('users').doc(rootState.users.currentUser.uid)
    let vehicles = await getVehicles(userRef)
    commit(types.SET_VEHICLE_CURRENT_USER, vehicles)
  },
  setNewUserInfo ({ rootState }, { uName, fName, lName, bDate }) {
    let url = process.env.NODE_API + 'setnewuser'
    bDate.data = moment(bDate.data, 'YYYY-MM-DD')
    bDate.data = (bDate.data.isValid() ? bDate.data.toDate() : null)
    let body = { uName, fName, lName, bDate }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
        .catch(err => {
          console.log('err', err.response)
          return Promise.reject(err.response)
        })
      })
  },
  updateUserInfo ({ rootState }, { fName, lName, bDate, tel, address, refUsers }) {
    let url = process.env.NODE_API + 'updateuser'
    let body = { fName, lName, bDate, tel, address, refUsers }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
        .catch(err => {
          console.log('err', err.response)
          return Promise.reject(err.response)
        })
      })
  },
  uploadAvatar ({ rootState, state }, { image, progressCb, errCb, completeCb }) {
    let avatarRef = rootState.firebase.storage().ref('images/' + state.currentUser.uid + '/profile.jpg')
    image = new Blob([image], { type: 'image/jpeg' })
    let task = avatarRef.put(image)
    task.on('state_changed',
      (snapshot) => {
        let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        progressCb(percentage)
      },
      (err) => {
        errCb(err)
      },
      () => {
        // get request to get URL for uploaded file
        avatarRef.getDownloadURL().then((url) => {
          rootState.firestore.collection('users').doc(state.currentUser.uid).collection('info').doc('public').set({
            avatar: url
          }, { merge: true })
            .then(() => {
              completeCb(url)
            })
            .catch(err => { throw (err) })
        })
      }
    )
  },
  uploadImage ({ rootState, state }, { img, progressCb, errCb, completeCb }) {
    let date = new Date()
    let nameStamp = date.getTime()
    let docRef = rootState.firebase.storage().ref('images/' + state.currentUser.uid + '/' + nameStamp + '.jpg')
    uploadImage({ img, docRef, progressCb, errCb, completeCb })
  }
}

function uploadImage ({ img, docRef, progressCb, errCb, completeCb }) {
  img = new Blob([img], { type: 'image/jpeg' })
  let task = docRef.put(img)
  task.on('state_changed',
    (snapshot) => {
      let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      progressCb(percentage)
    },
    (err) => {
      errCb(err)
    },
    () => {
      // get request to get URL for uploaded file
      docRef.getDownloadURL().then((url) => {
        completeCb(url)
      }).catch((err) => {
        errCb(err)
      })
    }
  )
}
function getPublicInfo (userRef) {
  return userRef.collection('info').doc('public').get()
    .then(doc => {
      if (doc.exists) {
        return doc.data()
      } else {
        return null
      }
    })
}
function getPrivateInfo (userRef) {
  return userRef.collection('info').doc('private').get()
    .then(doc => {
      if (doc.exists) {
        return doc.data()
      } else {
        return null
      }
    })
}
function getVehicles (userRef) {
  let vehicles = []
  let vehicleGroups = []
  return userRef.collection('vehicles').get()
    .then(docs => {
      if (docs.size > 0) {
        docs.forEach((doc) => { vehicles.push({ id: doc.id, ...doc.data() }) })
      }
      let grpObjs = vehicles.reduce((groups, item) => {
        var val = item['type']
        groups[val] = groups[val] || { type: item['type'], items: [] }
        groups[val].items.push(item)
        return groups
      }, {})
      for (let prop in grpObjs) {
        vehicleGroups.push(grpObjs[prop])
      }
      return vehicleGroups
    })
}
