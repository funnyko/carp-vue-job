import orderBy from 'lodash/orderBy'

export default {
  vehicleWithHeaderForUI (state) {
    let items = []
    state.currentUser.vehicles.forEach((group, index) => {
      if (index !== 0) {
        items.push({ divider: true })
      }
      items.push({ header: group.type })
      for (let iIndex in group.items) {
        items.push(group.items[iIndex])
      }
    })
    return items
  },
  activeLatestHitchRoute (state, getters, rootState) {
    let items = []
    let hitchActive = filterObject(rootState.hitchs.all, 'status', 'active')
    let routeActive = filterObject(rootState.routes.all, 'status', 'active')
    let actives = hitchActive.concat(routeActive)
    actives = orderBy(actives, ['created'], ['desc'])
    actives.forEach((item, index) => {
      if (index !== 0) {
        items.push({ divider: true })
      }
      items.push(item)
    })
    return items
  },
  completeLatestHitchRoute (state, getters, rootState) {
    let items = []
    let hitchComplete = filterObject(rootState.hitchs.all, 'status', 'done')
    let routeComplete = filterObject(rootState.routes.all, 'status', 'done')
    let completes = hitchComplete.concat(routeComplete)
    completes = orderBy(completes, ['created'], ['desc'])
    completes.forEach((item, index) => {
      if (index !== 0) {
        items.push({ divider: true })
      }
      items.push(item)
    })
    return items
  }
}

function filterObject (object, filterField, filterVal) {
  let items = []
  for (const key in object) {
    if (object[key][filterField] === filterVal) {
      items.push(object[key])
    }
  }
  return items
}
