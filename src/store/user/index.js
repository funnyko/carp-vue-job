import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import vehicles from './vehicle/index.js'

Vue.use(Vuex)

const state = {
  all: {},
  currentUser: {}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    vehicles
  },
  plugins: []
}
