import * as firebase from 'firebase'

export default {
  createVehicle ({ commit, dispatch, rootState }, { pId, img, name, desc, nSeat, color, type }) {
    pId = pId.trim().replace(' ', '-')
    let docRef = rootState.firestore.collection('users').doc(rootState.users.currentUser.uid).collection('vehicles').doc(pId)
    let vehicle = { /*censored*/ }
    return docRef.set(vehicle)
      .then(doc => {
        return dispatch('users/refreshVehicles', null, { root: true })
          .then(() => {
            return true
          })
      })
      .catch(err => { throw (err) })
  }
}
