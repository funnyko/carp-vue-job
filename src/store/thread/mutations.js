import Vue from 'vue'
import * as types from './mutation-types'
import moment from 'moment'

export default {
  [types.SET_THREAD] (state, thread) {
    // create new thread if the thread doesn't exist
    if (!state.all[thread.id]) {
      createThread(state, thread)
    }
  },
  [types.RECEIVE_MESSAGE] (state, { threadId, message }) {
    console.log('get message')
    if (!state.allMsgIds.includes(message.id)) {
      let data = message.data()
      state.all[threadId].messages.push({
        id: message.id,
        author: data.from,
        text: data.msg
      })
      state.allMsgIds.push(message.id)
    }
  },
  [types.SWITCH_THREAD] (state, { threadId }) {
    state.currentThreadId = threadId
    state.currentThread = {
      id: threadId,
      data: state.all[threadId] ? state.all[threadId] : null
    }
  },
  [types.SET_LATEST_DEAL] (state, { threadId, doc }) {
    let data = doc.data()
    // The behavior for Date objects stored in Firestore is going to change
    data.sTime = (typeof data.sTime != 'undefined') ? data.sTime.toDate() : ''
    data.eTime = (typeof data.eTime != 'undefined') ? data.eTime.toDate() : ''
    if (data.created) data.created = data.created.toDate()
    if (moment.isDate(data.sTime)) {
      data.sDate = moment(data.sTime).format('YYYY-MM-DD')
      data.sTime = moment(data.sTime).format('HHmm')
    }
    if (moment.isDate(data.eTime)) {
      data.eDate = moment(data.eTime).format('YYYY-MM-DD')
      data.eTime = moment(data.eTime).format('HHmm')
    }
    data.points.src.latLng = {
      lat: data.points.src.latLng[0],
      lng: data.points.src.latLng[1]
    }
    data.points.dest.latLng = {
      lat: data.points.dest.latLng[0],
      lng: data.points.dest.latLng[1]
    }
    state.all[threadId].refData = {
      ...state.all[threadId].refData,
      latestDeal: {
        ref: doc,
        data
      }
    }
  },
  [types.SET_CURRENT_THREAD_ERROR] (state, { err }) {
    state.currentThread.error = err
  },
  [types.SET_CURRENT_THREAD_LOADING] (state, loading) {
    console.log('loading', loading)
    state.currentThread.loading = loading
  },
  [types.UNSUBSCRIBE_THREAD] (state) {
    if (state.unsubscribe !== null) {
      state.unsubscribe()
      state.unsubscribe = null
    }
  }
}

function createThread (state, thread) {
  let threadData = thread.data()

  try {
    console.log('threadData', threadData)
    Vue.set(state.all, thread.id, {
      id: thread.id,
      title: thread.id,
      messages: [],
      users: threadData.users,
      lastMessage: null,
      refData: threadData.refData,
      deals: []
    })
  } catch (err) {
    console.log(err.message, err.name)
  }
}
