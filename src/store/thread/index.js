import * as getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  all: {},
  allIds: {
    routes: {
      title: 'routes',
      ids: []
    },
    deals: {
      p2d: {
        title: 'p2d',
        ids: []
      },
      d2p: {
        title: 'd2p',
        ids: []
      }
    }
  },
  allMsgIds: [],
  currentThread: {},
  unsubscribe: null
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  plugins: []
}
