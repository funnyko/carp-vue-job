import * as types from './mutation-types'
import * as firebase from 'firebase'

export default {
  async getAll ({ commit, rootState }) {
    let roomsCol = rootState.firestore.collection('threads')
      .where('users.' + rootState.users.currentUser.uid, '==', true)
    let rooms = await roomsCol.get()
    rooms.forEach(room => commit(types.SET_THREAD, room))
  },
  async getThreadByDealId ({ commit, dispatch, state, rootState }, dealId) {
    let thread = getInStateByDealId({state}, dealId)
    if (thread) {
      return thread
    }
    let querySnap = rootState.firestore.collection('threads')
      .where('refData.dealId', '==', dealId)
      .where('users.' + rootState.users.currentUser.uid, '==', true)
    thread = await querySnap.get()
      .then((snapshot) => {
        if (snapshot.size > 0) {
          commit(types.SET_THREAD, snapshot.docs[0])
          return getInStateByDealId({state}, dealId)
        } else {
          return { err: 'Thread not found.' }
        }
      }).catch(err => { throw (err) })
    return thread
  },
  async getThreadByThreadId ({ commit, dispatch, state, rootState }, threadId) {
    if (state.all[threadId]) {
      return state.all[threadId]
    } else {
      let thread = await getThreadByThreadId({ commit, dispatch, state, rootState }, { threadId })
      if (!thread.err) {
        commit(types.SET_THREAD, thread)
        return state.all[threadId]
      } else {
        return thread
      }
    }
  },
  async sendMessage ({ commit, rootState }, { text, created, threadId }) {
    const messagesRef = rootState.firestore.collection('threads').doc(threadId).collection('messages')
    messagesRef.add({
      created: firebase.firestore.FieldValue.serverTimestamp(),
      /*censored*/
    })
      .then(res => { return true })
      .catch(err => { throw (err) })
  },
  async switchThread ({ commit, dispatch, state, rootState }, { threadId, latestDealCb }) {
    commit(types.SWITCH_THREAD, { threadId: null })
    commit(types.SET_CURRENT_THREAD_LOADING, true)
    if (state.all[threadId]) {
      commit(types.UNSUBSCRIBE_THREAD)
      if (state.all[threadId].refData.type === 'p2d' || state.all[threadId].refData.type === 'd2p') {
        subLastestDeal({ commit, dispatch, state, rootState }, threadId, latestDealCb)
      }
      state.unsubscribe = rootState.firestore.collection('threads').doc(threadId).collection('messages').orderBy('created')
        .onSnapshot(querySnapshot => {
          let source = querySnapshot.metadata.hasPendingWrites ? 'Local' : 'Server'
          querySnapshot.forEach(function (message) {
            commit(types.RECEIVE_MESSAGE, {
              threadId, message
            })
          })
        })
    }
  }
}

function getInStateByDealId ({state}, dealId) {
  for (const key in state.all) {
    const el = state.all[key]
    if (el.refData.dealId === dealId) {
      return el
    }
  }
  return null
}

async function getThreadByThreadId ({ commit, dispatch, state, rootState }, { threadId }) {
  let threadRef = rootState.firestore.collection('threads').doc(threadId)
  let thread = await threadRef.get()
    .then((doc) => {
      if (doc.exists) {
        return doc
      } else {
        return { err: 'Thread not found.' }
      }
    }).catch(err => { throw (err) })
  return thread
}
async function subLastestDeal ({ commit, dispatch, state, rootState }, threadId, cb) {
  let deal = await dispatch('deals/get', { dealId: state.all[threadId].refData.dealId }, { root: true })
  state.all[threadId].refData['dealData'] = {...deal}
  commit(types.SWITCH_THREAD, { threadId })
  commit(types.SET_CURRENT_THREAD_LOADING, false)
  state.all[threadId].currentDeal = rootState.firestore.collection('deals').doc(deal.id).collection('subDeals').orderBy('created', 'desc').limit(1)
    .onSnapshot(function (snapshot) {
      snapshot.forEach(function (doc) {
        commit(types.SET_LATEST_DEAL, { threadId, doc })
        if (cb) {
          cb(state.all[threadId].refData.latestDeal)
        }
      })
    })
}
