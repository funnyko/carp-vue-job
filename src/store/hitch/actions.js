import * as types from './mutation-types'
// import * as firebase from 'firebase'
import moment from 'moment'
import axios from 'axios'

export default {
  async getAll ({ commit, rootState }) {
    let hitchsCol = rootState.firestore.collection('hitchs')
      .where('owner', '==', rootState.users.currentUser.uid)
    let hitchs = await hitchsCol.get()
    hitchs.forEach(hitch => commit(types.SET_HITCH, hitch))
  },
  async get ({ commit, state, rootState }, hitchId) {
    if (!state.all[hitchId]) {
      let hitch = await getHitchByHitchId({ commit, state, rootState }, hitchId)
      if (!hitch.err) {
        commit(types.SET_HITCH, hitch)
      }
    }
    return state.all[hitchId] ? state.all[hitchId] : { err: 'hitch not found' }
  },
  async getDeals ({ commit, state, rootState, dispatch }, hitchId) {
    if (state.all[hitchId].requests) {
      return state.all[hitchId].requests
    } else {
      let requests = await getDealsByHitchId({ commit, state, rootState, dispatch }, hitchId)
      if (!requests.err) {
        commit(types.SET_HITCH_DEAL, { hitchId, requests })
      }
      return state.all[hitchId].requests
    }
  },
  create ({ commit, rootState }, payload) {
    let url = process.env.NODE_API + 'createhitch'
    let sTime = toDateTime(payload.hitch.sDate, payload.hitch.sTime)
    let eTime = toDateTime(payload.hitch.eDate, payload.hitch.eTime)
    let data = {
      src: {
        /*censored*/
      },
      dest: {
        /*censored*/
      },
      hitch: {
        /*censored*/
      }
    }
    // if withDeal
    if (payload.deal) {
      data['deal'] = payload.deal
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, data, { headers: headers })
        .catch(err => {
          console.log('err', err.response)
          return Promise.reject(err.response)
        })
      })
  },
  createExistP2d ({commit, rootState}, payload) {
    let url = process.env.NODE_API + 'existp2d'
    let srcCoord = [payload.points.src.latLng.lat, payload.points.src.latLng.lng]
    let destCoord = [payload.points.dest.latLng.lat, payload.points.dest.latLng.lng]
    let sTime = toDateTime(payload.deal.sDate, payload.deal.sTime)
    let eTime = toDateTime(payload.deal.eDate, payload.deal.eTime)
    console.log('payload', payload)
    let body = {
      /*censored*/
      offer: {
        /*censored*/
      }
    }
    return rootState.firebase.auth().currentUser.getIdToken()
      .then(authToken => {
        const headers = { Authorization: 'Bearer ' + authToken }
        return axios.post(url, body, { headers: headers })
      })
  }
}

async function getHitchByHitchId ({ commit, state, rootState }, hitchId) {
  return rootState.firestore.collection('hitchs').doc(hitchId).get()
    .then(doc => {
      if (doc.exists) {
        return doc
      } else {
        return { err: 'hitch doesn\'t exist.' }
      }
    }).catch(function (error) {
      return { err: error }
    })
}

async function getDealsByHitchId ({ commit, state, rootState, dispatch }, hitchId) {
  let requests = []
  return rootState.firestore.collection('deals').where('users.' + rootState.users.currentUser.uid, '==', 'p').where('refs.hitchId', '==', hitchId).get()
    .then(docs => {
      if (docs.size > 0) {
        docs.forEach(async (doc) => {
          let driverId = getKeyByValue(doc.data().users, 'd')
          let driver = await dispatch('users/get', { userId: driverId }, { root: true })
          requests.push({ id: doc.id, ...doc.data(), driver })
        })
      }
      return requests
    }).catch(function (error) {
      return { err: error }
    })
}
