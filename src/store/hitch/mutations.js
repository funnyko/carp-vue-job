import Vue from 'vue'
import * as types from './mutation-types'
import moment from 'moment'

export default {
  [types.SET_HITCH] (state, hitch) {
    // create new hitch if the hitch doesn't exist
    if (!state.all[hitch.id]) {
      let data = hitch.data()
      if (data.status === 'active') {
        // The behavior for Date objects stored in Firestore is going to change
        data.sTime = (typeof data.sTime != 'undefined') ? data.sTime.toDate() : ''
        data.eTime = (typeof data.eTime != 'undefined') ? data.eTime.toDate() : ''
        data.created = data.created.toDate()
        if (moment.isDate(data.sTime)) {
          data.sDate = moment(data.sTime).format('YYYY-MM-DD')
          data.sTime = moment(data.sTime).format('HH:mm')
        }
        if (moment.isDate(data.eTime)) {
          data.eDate = moment(data.eTime).format('YYYY-MM-DD')
          data.eTime = moment(data.eTime).format('HH:mm')
        }
        data.points.src.latLng = {
          lat: data.points.src.latLng[0],
          lng: data.points.src.latLng[1]
        }
        data.points.dest.latLng = {
          lat: data.points.dest.latLng[0],
          lng: data.points.dest.latLng[1]
        }
      }
      createHitch(state, {
        id: hitch.id,
        ...data
      })
    }
  },
  [types.SET_HITCH_DEAL] (state, { hitchId, deals }) {
    state.all[hitchId].deals = deals
  }
}

function createHitch (state, hitch) {
  Vue.set(state.all, hitch.id, {
    type: 'hitch',
    link: { name: 'hitchInfo', params: { hitchId: hitch.id } },
    ...hitch
  })
}
