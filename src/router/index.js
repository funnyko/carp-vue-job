import Vue from 'vue'
import Router from 'vue-router'
import GoogleMap from '@/components/GoogleMap'
import Home from '@/components/Home'
import P404 from '@/components/404'
import CreateRoute from '@/components/drives/CreateRoutePage'
import ExistRoutePage from '@/components/drives/ExistRoutePage'
import EditDeal from '@/components/deals/EditDeal'
import CreateHitch from '@/components/hitchs/CreateHitchPage'
import ExistHitch from '@/components/hitchs/ExistHitchPage'
import BtmNav from '@/components/BtmNav'
import DriverRating from '@/components/ratings/DriverRating'
import PassengerRating from '@/components/ratings/PassengerRating'
import Auth from '@/components/users/Auth'
import NewUser from '@/components/users/NewUser'
import EditInfo from '@/components/users/EditInfo'
import Vehicles from '@/components/users/vehicles/Vehicles'
import CreateVehicle from '@/components/users/vehicles/CreateVehicle'
// import ThreadsDialog from '@/components/dialogs/thread/ThreadsDialog'
// import ThreadContainerDialog from '@/components/dialogs/thread/ThreadContainerDialog'
import Thread from '@/components/chats/Thread'
import SearchLocation from '@/components/childMaps/SearchLocation.vue'
import { store } from '../store'
import * as firebase from 'firebase'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/', redirect: '/home'
    },
    {
      path: '/home',
      components: { default: Home, btmnav: BtmNav },
      meta: { requiresAuth: true }
    },
    { path: '/404', name: '404', component: P404, props: true },
    {
      path: '/thread/:threadId',
      name: 'thread',
      components: { default: GoogleMap, overlay: Thread, btmnav: BtmNav },
      meta: { requiresAuth: true }
    },
    {
      path: '/thread/deal/:dealId',
      name: 'threadByDealId',
      components: { default: GoogleMap, overlay: Thread, btmnav: BtmNav },
      meta: { requiresAuth: true }
    },
    {
      path: '/route',
      name: 'createRoute',
      components: { default: GoogleMap, overlay: CreateRoute, btmnav: BtmNav },
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        console.log('store.state.users.currentUser.vehicles', store.state.users.currentUser.vehicles)
        if (store.state.users.currentUser.vehicles.length === 0) {
          next({
            path: '/vehicles/create',
            query: { redirect: to.fullPath }
          })
        }
        next()
      }
    },
    {
      path: '/route/:routeId',
      name: 'routeInfo',
      components: { default: GoogleMap, overlay: ExistRoutePage, btmnav: BtmNav }
    },
    {
      path: '/hitch',
      components: { default: GoogleMap, overlay: CreateHitch, btmnav: BtmNav }
    },
    {
      path: '/hitch/:hitchId',
      name: 'hitchInfo',
      components: { default: GoogleMap, overlay: ExistHitch, btmnav: BtmNav }
    },
    {
      path: '/editdeal/:dealId',
      name: 'editDeal',
      components: { default: GoogleMap, overlay: EditDeal, btmnav: BtmNav }
    },
    // {
    //   path: '/driving',
    //   components: { default: { template: '<div>driving</div>' }, btmnav: BtmNav }
    // },
    { path: '/map',
      name: 'index',
      component: GoogleMap,
      children: [
        { path: '', component: { template: '<div></div>' } }
      ]
    },
    { path: '/login', name: 'login', component: Auth },
    { path: '/editinfo', component: EditInfo, meta: { requiresAuth: true } },
    { path: '/newuser',
      component: NewUser,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        if (store.state.users.currentUser.uName) {
          // console.log(store.state.users.currentUser)
          next({
            path: '/home'
          })
        }
        next()
      }
    },
    { path: '/vehicles', components: { default: Vehicles, btmnav: BtmNav }, meta: { requiresAuth: true } },
    { path: '/vehicles/create', name: 'createVihecle', component: CreateVehicle, meta: { requiresAuth: true } },
    // { path: '/threads', name: 'threads', component: ThreadsDialog },
    // { path: '/threads/:threadId', name: 'thread', component: ThreadContainerDialog },
    { path: '/rating/driver/:rateId', name: 'driverRating', component: DriverRating, props: true },
    { path: '/rating/passenger/:rateId', name: 'passengerRating', component: PassengerRating, props: true },
    { path: '/searchLocation', name: 'searchLocation', component: SearchLocation, props: true }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    // console.log(firebase.auth().currentUser)
    if (!firebase.auth().currentUser) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      // check if public undefied (public contain username)
      if (!store.state.users.currentUser.uName && to.path !== '/newuser') {
        next({
          path: '/newuser'
        })
      }
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
