# waypun
Carpooling (also known as car-sharing, ride-sharing, lift-sharing and covoiturage).

## Status: Researching blockchain...

  This repository it only shows the way how I coding that can't be build&run. You can go to https://carp-222.firebaseapp.com , [repo/node](https://bitbucket.org/funnyko/carp-vue-job/src/master/node/) and [repo/docs](https://bitbucket.org/funnyko/carp-vue-job/src/master/docs/) to check how the application works or execute below commands to see some automated testing.

## Users & Password
- test1@waypun.com 123456
- test2@waypun.com 123456
- test3@waypun.com 123456

## Automated Testing
``` bash
# install dependencies
npm install

# run e2e tests
npm run e2e
```
