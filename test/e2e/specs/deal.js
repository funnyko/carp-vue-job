
module.exports = {
  before : function(client) {
    const loginPage = client.page.loginPage();
    loginPage.navigate()
      .login('test_hitchhike@waypun.com', '123456')
  },

  'send message': function (client) {
    const dealChatPage = client.page.dealChatPage();
    const message = 'test message'

    dealChatPage
      .navigate()
      .waitForElementVisible('.msg', 10000)
      .sendMessage(message)
  },

  'make a deal': function (client) {
    const dealEditPage = client.page.dealEditPage();
    const message = 'test message'
    const makeDealDialogSection = dealEditPage.section.makeDealDialog
    
    dealEditPage
      .navigate()
      .waitForElementVisible('@makeDealBtn', 10000)
      .click('@makeDealBtn')
    
    dealEditPage.expect.section('@makeDealDialog').to.be.visible.before(5000)
    client.pause(2000) // for watching
    makeDealDialogSection.assert.attributeContains('@submitBtn', 'disabled', true)
    makeDealDialogSection.setValue('@messageInput', message)
    makeDealDialogSection.expect.element('@submitBtn').to.not.have.attribute('disabled')
  }
}
