// // For authoring Nightwatch tests, see
// // http://nightwatchjs.org/guide#usage

// module.exports = {
//   before : function(client) {
//     const loginPage = client.page.loginPage();
//     loginPage.navigate()
//       .login('testroute@waypun.com', '123456')
//   },

//   after : function(client) {
//     // client.end();
//   },
//   'create route tests': function (client) {
//     const createRoutePage = client.page.createRoutePage();
//     const datepicker = client.page.datepicker();
//     const inputLocation = client.page.inputLocation();
//     const createRouteDialogSection = createRoutePage.section.createRouteDialog
//     const directionOptionsDialogSection = createRoutePage.section.directionOptionsDialog
//     let curDate = (new Date()).getDate();

//     createRoutePage
//       .navigate()
//       .waitForElementVisible('div[data-app=true]', 10000)
//     inputLocation.seachAndSelectPlace('@startInput', 'paragon', 'Paragon Cineplex')
//     inputLocation.seachAndSelectPlace('@destinationInput', 'central world', 'Central World')
//     createRoutePage
//       .waitForElementVisible('@findDirectionsBtn', 1000)
//       .click('@findDirectionsBtn')
//     createRoutePage.expect.section('@directionOptionsDialog').to.be.visible.before(1000)    
//     client.pause(500)
//     directionOptionsDialogSection
//       .waitForElementVisible('@googleDirectionBtn', 2000)
//       .click('@googleDirectionBtn')
      

//     createRoutePage
//       .waitForElementVisible('@createRouteBtn', 3000)  
//       .click('@createRouteBtn')
//     createRoutePage.expect.section('@createRouteDialog').to.be.visible.before(1000)
//     client.pause(1000)
//     createRouteDialogSection.expect.element('@submitBtn').to.be.visible.before(1000)
//     createRouteDialogSection.click('@submitBtn')
//     // client.pause(1000)        
//     createRouteDialogSection.expect.element('@titleInputMsgXpath').text.to.contain('Required').before(1000)
//     createRouteDialogSection.expect.element('@vehicleInputMsgXpath').text.to.contain('Required').before(1000)
//     createRouteDialogSection.expect.element('@priceInputMsgXpath').text.to.contain('Required').before(1000)
//     createRouteDialogSection.selectVehicle(0)
//     // client.pause(1500)        
//     createRouteDialogSection
//       .setValue('@titleInput', 'TestTitle')
//       .setValue('@priceInput', '123')
//       .setValue('@departureTimeInput', '2222')
//       .setValue('@arrivalTimeInput', '2222')
//     datepicker
//       .pick('departure-date', curDate)
//       .pick('arrival-date', curDate+1)
//   },

//   'create hitchhike tests': function (client) {
//     const createHitchhikePage = client.page.createHitchhikePage();
//     const datepicker = client.page.datepicker();
//     const inputLocation = client.page.inputLocation();
//     const createHitchhikeDialogSection = createHitchhikePage.section.createHitchhikeDialog
//     let curDate = (new Date()).getDate();

//     createHitchhikePage
//       .navigate()
//       .waitForElementVisible('div[data-app=true]', 10000)
//     inputLocation.seachAndSelectPlace('@startInput', 'paragon', 'Paragon Cineplex')
//     inputLocation.seachAndSelectPlace('@destinationInput', 'central world', 'Central World')
//     // client.pause(500)
//     createHitchhikePage
//       .waitForElementVisible('@createHitchhikeBtn', 2000)
//       .click('@createHitchhikeBtn')

//     createHitchhikePage.expect.section('@createHitchhikeDialog').to.be.visible.before(1000)
//     // client.pause(1000)
//     createHitchhikeDialogSection.click('@submitBtn')
//     // client.pause(1000)
//     createHitchhikeDialogSection.expect.element('@titleInputMsgXpath').text.to.contain('Required')
//     createHitchhikeDialogSection.expect.element('@priceInputMsgXpath').text.to.contain('Required')
//     // client.pause(1500)
//     createHitchhikeDialogSection
//       .setValue('@titleInput', 'TestTitle')
//       .setValue('@priceInput', '123')
//       .setValue('@pickupTimeInput', '2222')
//       .setValue('@arrivalTimeInput', '2222')
//     datepicker
//       .pick('pickup-date', curDate)
//       .pick('arrival-date', curDate+1)
//   }
// }
