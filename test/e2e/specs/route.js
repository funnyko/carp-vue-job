module.exports = {
  before : function(client) {
    const loginPage = client.page.loginPage();
    loginPage.navigate()
      .login('testroute@waypun.com', '123456')
  },
  after : function(client) {
    // client.end();
  },
  'create route': function (client) {
    const createRoutePage = client.page.createRoutePage();
    const datepicker = client.page.datepicker();
    const inputLocation = client.page.inputLocation();
    const createRouteDialogSection = createRoutePage.section.createRouteDialog
    const directionOptionsDialogSection = createRoutePage.section.directionOptionsDialog
    let curDate = (new Date()).getDate();

    createRoutePage
      .navigate()
      .waitForElementVisible('div[data-app=true]', 10000)
    inputLocation.seachAndSelectPlace('@startInput', 'central world', 'Central World')
    inputLocation.seachAndSelectPlace('@destinationInput', 'paragon', 'Paragon Cineplex')
    createRoutePage
      .waitForElementVisible('@findDirectionsBtn', 1000)
      .click('@findDirectionsBtn')
    createRoutePage.expect.section('@directionOptionsDialog').to.be.visible.before(1000)    
    client.pause(500)
    directionOptionsDialogSection
      .waitForElementVisible('@googleDirectionBtn', 2000)
      .click('@googleDirectionBtn')
      

    createRoutePage
      .waitForElementVisible('@createRouteBtn', 3000)  
      .click('@createRouteBtn')
    createRoutePage.expect.section('@createRouteDialog').to.be.visible.before(1000)
    client.pause(1000)
    createRouteDialogSection.expect.element('@submitBtn').to.be.visible.before(1000)
    createRouteDialogSection
      .click('@submitBtn')
    createRouteDialogSection.expect.element('@titleInputMsgXpath').text.to.contain('Required').before(1000)
    createRouteDialogSection.expect.element('@vehicleInputMsgXpath').text.to.contain('Required').before(1000)
    createRouteDialogSection.expect.element('@priceInputMsgXpath').text.to.contain('Required').before(1000)
    createRouteDialogSection.selectVehicle(0)
    createRouteDialogSection
      .setValue('@titleInput', 'TestTitle')
      .setValue('@priceInput', '123')
      .setValue('@departureTimeInput', '2222')
      .setValue('@arrivalTimeInput', '2222')
    datepicker
      .pick('departure-date', curDate)
      .pick('arrival-date', curDate+1)
    createRouteDialogSection
      .click('@closeDialogBtn')
    createRoutePage.expect.section('@createRouteDialog').to.be.not.visible.before(2000)
  },

  'find matched hitchhike': function(client) {
    const createRoutePage = client.page.createRoutePage();
    const findOptionsDialogSection = createRoutePage.section.findOptionsDialog

    createRoutePage
      .waitForElementVisible('@findHitchhikesBtn', 2000)
      .click('@findHitchhikesBtn')
    createRoutePage.expect.section('@findOptionsDialog').to.be.visible.before(1000)
    client.pause(1000)
    findOptionsDialogSection
      .setValue('@pickupDistanceInput', '0')
      .setValue('@destinationDistanceInput', '0')
      .click('@submitBtn')
    createRoutePage.expect.section('@findOptionsDialog').to.be.not.visible.before(1000)
    createRoutePage.assert.cssClassPresent('@findHitchhikesBtn', 'btn--loader')
    createRoutePage.expect.section('@matchedHitchhikeTabs').to.be.visible.before(20000)
  },

  'create route with hitchhike': function(client) {
    const createRoutePage = client.page.createRoutePage();
    const createRouteDialogSectionSection = createRoutePage.section.createRouteDialog
    const matchedHitchhikeTabsSection = createRoutePage.section.matchedHitchhikeTabs
    
    matchedHitchhikeTabsSection.expect.element('@firstMatchedLink').to.be.visible.before(20000)
    matchedHitchhikeTabsSection
      .click('@firstMatchedLink')
    matchedHitchhikeTabsSection.expect.element('@firstMatchedMakeDealBtn').to.be.visible.before(5000)
    matchedHitchhikeTabsSection
      .click('@firstMatchedMakeDealBtn')
    
    createRoutePage.expect.section('@createRouteDialog').to.be.visible.before(1000)
    client.pause(1000)
    createRouteDialogSectionSection
      .click('@submitBtn')
    createRouteDialogSectionSection.expect.element('@dealMessageInputMsgXpath').text.to.contain('Required').before(1000)
    createRouteDialogSectionSection.setValue('@dealMessageInput', 'test message')
    createRouteDialogSectionSection.expect.element('@submitBtn').to.not.have.attribute('disabled')
    createRouteDialogSectionSection
      .click('@closeDialogBtn')
  }
}
