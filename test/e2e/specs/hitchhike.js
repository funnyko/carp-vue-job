module.exports = {
  before : function(client) {
    const loginPage = client.page.loginPage();
    loginPage.navigate()
      .login('test_hitchhike@waypun.com', '123456')
  },

  after : function(client) {
    // client.end();
  },

  'create hitchhike': function (client) {
    const createHitchhikePage = client.page.createHitchhikePage();
    const datepicker = client.page.datepicker();
    const inputLocation = client.page.inputLocation();
    const createHitchhikeDialogSection = createHitchhikePage.section.createHitchhikeDialog
    let curDate = (new Date()).getDate();

    createHitchhikePage
      .navigate()
      .waitForElementVisible('div[data-app=true]', 10000)
    inputLocation.seachAndSelectPlace('@startInput', 'MBK', 'MBK')
    inputLocation.seachAndSelectPlace('@destinationInput', 'central world', 'Central World')

    createHitchhikePage
      .waitForElementVisible('@createHitchhikeBtn', 2000)
      .click('@createHitchhikeBtn')

    createHitchhikePage.expect.section('@createHitchhikeDialog').to.be.visible.before(1000)
    client.pause(1000)
    createHitchhikeDialogSection.click('@submitBtn')
    createHitchhikeDialogSection.expect.element('@titleInputMsgXpath').text.to.contain('Required').before(1000)
    createHitchhikeDialogSection.expect.element('@priceInputMsgXpath').text.to.contain('Required').before(1000)
    createHitchhikeDialogSection
      .setValue('@titleInput', 'TestTitle')
      .setValue('@priceInput', '123')
      .setValue('@pickupTimeInput', '2222')
      .setValue('@arrivalTimeInput', '2222')
    datepicker
      .pick('pickup-date', curDate)
      .pick('arrival-date', curDate+1)
    createHitchhikeDialogSection
      .click('@closeDialogBtn')
    createHitchhikePage.expect.section('@createHitchhikeDialog').to.be.not.visible.before(2000)      
  },

  'find matched route': function(client) {
    const createHitchhikePage = client.page.createHitchhikePage();
    const findOptionsDialogSection = createHitchhikePage.section.findOptionsDialog

    createHitchhikePage
      .waitForElementVisible('@findDriversBtn', 2000)
      .click('@findDriversBtn')
    createHitchhikePage.expect.section('@findOptionsDialog').to.be.visible.before(1000)
    client.pause(1000)
    findOptionsDialogSection
      .setValue('@pickupDistanceInput', '0')
      // .value('@pickupDistanceInput', '1000')
      .setValue('@destinationDistanceInput', '0')
      // .value('@destinationDistanceInput', '1000')
      .click('@submitBtn')
    createHitchhikePage.expect.section('@findOptionsDialog').to.be.not.visible.before(1000)
    createHitchhikePage.assert.cssClassPresent('@findDriversBtn', 'btn--loader')
    createHitchhikePage.expect.element('@firstMatchedRouteList').to.be.visible.before(20000)
    createHitchhikePage.click('@firstMatchedRouteList')
  },

  'create hitchhike with route': function(client) {
    const createHitchhikePage = client.page.createHitchhikePage();
    const createHitchhikeDialogSection = createHitchhikePage.section.createHitchhikeDialog
    const matchedRouteTabsSection = createHitchhikePage.section.matchedRouteTabs
    
    createHitchhikePage.expect.section('@matchedRouteTabs').to.be.visible.before(2000)
    matchedRouteTabsSection.expect.element('@firstMatchedMakeDealBtn').to.be.visible.before(5000)
    client.pause(2000) // for watching    
    matchedRouteTabsSection
      .click('@firstMatchedMakeDealBtn')
    createHitchhikePage.expect.section('@createHitchhikeDialog').to.be.visible.before(1000)
    client.pause(1000)
    createHitchhikeDialogSection
      .click('@submitBtn')
    createHitchhikeDialogSection.expect.element('@dealMessageInputMsgXpath').text.to.contain('Required').before(1000)
    createHitchhikeDialogSection.setValue('@dealMessageInput', 'test message')
    createHitchhikeDialogSection.expect.element('@submitBtn').to.not.have.attribute('disabled')
    createHitchhikeDialogSection
      .click('@closeDialogBtn')
  }
}
