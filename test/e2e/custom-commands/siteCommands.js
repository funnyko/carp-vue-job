var siteCommands = {
  login: function() {
    const devServer = this.api.globals.devServerURL

    return this.url(devServer+'/login')
      .waitForElementVisible('.firebaseui-idp-list', 5000)
      .click('@emailLoginBtn')
      // .setValue('input[type=password]', password)
      .waitForElementVisible('@submitButton')
      .setValue('@emailInput', 'nightwatch')
      // .assert.elementPresent('.dashboard')
  }
}

module.exports = {
  commands: [siteCommands],
  elements: {
    emailLoginBtn: {
      selector: 'button[data-provider-id=password]'
    },
    emailInput: {
      selector: 'input[type=email]'
      // selector: 'button[data-provider-id=password]'
    }
  }
};

