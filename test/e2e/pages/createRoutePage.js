module.exports = {
  url: function() { 
    return this.api.globals.prodServerURL + '/route';     
    // return this.api.globals.devServerURL + '/route'; 
  },
  commands: [
  ],
  sections: {
    directionOptionsDialog: {
      selector: '.dialog__direction-options',
      elements: {
        googleDirectionBtn: {
          selector: '#btn__google-directions'
        }
      }
    },
    createRouteDialog: {
      selector: '.dialog__create-route',
      commands: [{
        selectVehicle: function(n) {         
          this.click('#input__vehicle')
          this.api.useXpath()
          this.expect.element('@vehicleListOpeningXpath').to.be.visible.before(3000)
          this.api.useCss()
          this.expect.element('.vehicle--list').to.have.css('display').which.not.equals('none')
          this.click(`.vehicle--list .list__tile__title`)
          
          return this
        }
      }],
      elements: {
        closeDialogBtn: {
          selector: '#btn__close-route-dialog'
        },
        titleInput: {
          selector: '#input__title'
        },
        titleInputMsgXpath: {
          selector: `//input[contains(@id, 'input__title')]/../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath' 
        },
        vehicleInput: {
          selector: '#input__vehicle'
        },
        vehicleInputMsgXpath: {
          selector:  `//input[contains(@id, 'input__vehicle')]/../../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath'
        },
        vehicleListOpeningXpath: {
          selector:  `//input[contains(@id, 'input__vehicle')]/../../parent::div[contains(@class, 'input-group--open')]`,
          locateStrategy: 'xpath'
        },
        priceInput: {
          selector: '#input__price'
        },
        priceInputMsgXpath: {
          selector: `//input[contains(@id, 'input__price')]/../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath' 
        },
        dealMessageInput: {
          selector: '#input__deal-message'
        },
        dealMessageInputMsgXpath: {
          selector: `//input[contains(@id, 'input__deal-message')]/../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath' 
        },
        departureTimeInput: {
          selector: '#input__departure-time'
        },
        arrivalTimeInput: {
          selector: '#input__arrival-time'
        },
        submitBtn: {
          selector: '.btn__submit'
        }
      }
    },
    findOptionsDialog: {
      selector: '.dialog__find-matching-options',
      elements: {
        pickupDistanceInput: {
          selector: '#input__pickup-distance'
        },
        destinationDistanceInput: {
          selector: '#input__destination-distance'
        },
        submitBtn: {
          selector: '#btn__find-matching'
        }
      }
    },
    matchedHitchhikeTabs: {
      selector: '#tabs__matched-hitchhike',
      elements: {
        firstMatchedLink: {
          selector: `//a[contains(@class, 'tabs__item')][1]`,
          locateStrategy: 'xpath' 
        },
        firstMatchedMakeDealBtn: {
          selector: `//button[contains(@class, 'btn__make-deal')][1]`,
          locateStrategy: 'xpath' 
        }
      }
    }
  },
  elements: {
    findDirectionsBtn: {
      selector: '#btn__find-directions'
    },
    findHitchhikesBtn: {
      selector: '#btn__find-hitchhikes'
    },
    createRouteBtn: {
      selector: '#btn__create-route'
    }
  }
};