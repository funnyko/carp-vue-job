module.exports = {
  url: function() { 
    return this.api.globals.prodServerURL + '/hitch';     
  },
  commands: [],
  sections: {
    createHitchhikeDialog: {
      selector: '.dialog__create-hitchhike',
      elements: {
        closeDialogBtn: {
          selector: '#btn__close-hitch-dialog'
        },
        titleInput: {
          selector: '#input__title'
        },
        titleInputMsgXpath: {
          selector: `//input[contains(@id, 'input__title')]/../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath' 
        },
        priceInput: {
          selector: '#input__price'
        },
        priceInputMsgXpath: {
          selector: `//input[contains(@id, 'input__price')]/../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath' 
        },
        dealMessageInput: {
          selector: '#input__deal-message'
        },
        dealMessageInputMsgXpath: {
          selector: `//input[contains(@id, 'input__deal-message')]/../..//div[contains(@class, 'input-group__messages')]`,
          locateStrategy: 'xpath' 
        },
        pickupTimeInput: {
          selector: '#input__pickup-time'
        },
        arrivalTimeInput: {
          selector: '#input__arrival-time'
        },
        submitBtn: {
          selector: '.btn__submit'
        }
      }
    },
    findOptionsDialog: {
      selector: '.dialog__find-matching-options',
      elements: {
        pickupDistanceInput: {
          selector: '#input__pickup-distance'
        },
        destinationDistanceInput: {
          selector: '#input__destination-distance'
        },
        submitBtn: {
          selector: '#btn__find-matching'
        }
      }
    },
    matchedRouteTabs: {
      selector: '#tabs__matched-route',
      elements: {
        firstMatchedMakeDealBtn: {
          selector: `//button[contains(@class, 'btn__make-deal')][1]`,
          locateStrategy: 'xpath' 
        }
      }
    }
  },
  elements: {
    findDirectionsBtn: {
      selector: '#btn__find-directions'
    },
    googleDirectionBtn: {
      selector: '#btn__google-directions'
    },
    findDriversBtn: {
      selector: '#btn__find-drivers'
    },
    createHitchhikeBtn: {
      selector: '#btn__create-hitchhike'
    },
    firstMatchedRouteList: {
      selector: `//div[contains(@id, 'list__matched-route')]//div[contains(@class, 'list__tile')][1]`,
      locateStrategy: 'xpath' 
    }
  }
};