module.exports = {
  url: function() { 
    return this.api.globals.prodServerURL + '/thread/deal/PwGygfyVvkNA4URb4W3I'; 
  },
  commands: [{
    sendMessage: function(message) {
      const self = this
      const myMessagesSelector = '.msg.from-me' 

      return this.api.elements('css selector', myMessagesSelector, function (result) {
        self
          .click('@messageInput')
          // .setValue('@messageInput', [message, self.api.Keys.ENTER])
          .setValue('@messageInput', message)
          .click('@sendMessageBtn')
          .expect.element('@messageInput').text.to.equal('').before(1000)
        self.api.pause(2000) // for watching
        self.api
          .assert.elementCount(myMessagesSelector, result.value.length+1)

          return self
      })
    }
  }],
  elements: {
    messageInput: {
      selector: '#input__message'
    },
    sendMessageBtn: {
      selector: '#btn_send-chat-msg'
    }
  }
};