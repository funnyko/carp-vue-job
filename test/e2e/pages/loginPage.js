module.exports = {
  url: function() { 
    return this.api.globals.prodServerURL + '/login'; 
    // return this.api.globals.devServerURL + '/login'; 
  },
  commands: [{
    login: function(username, password) {
      this.waitForElementPresent('.firebaseui-idp-list', 5000)
      .click('@emailLoginBtn')
      .waitForElementPresent('@emailInput', 5000)
      .setValue('@emailInput', username)
      this.api.pause(500)
      this.click('@firebaseSubmit')
      .waitForElementPresent('@passwordInput', 5000)
      .setValue('@passwordInput', password)
      .click('@firebaseSubmit')

      this.api.pause(5000)
    }
  }],
  elements: {
    emailLoginBtn: {
      selector: 'button[data-provider-id=password]'
    },
    emailInput: {
      selector: 'input[type=email]'
    },
    passwordInput: {
      selector: 'input[type=password]'
    },
    firebaseSubmit: {
      selector: '.firebaseui-form-actions .firebaseui-id-submit'
    }
  }
};