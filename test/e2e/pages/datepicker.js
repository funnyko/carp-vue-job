module.exports = {
  commands: [{
    pick: function(elementName, date) {
      var elementInputSelector = `#input__${elementName}`
      var elementDialogSelector = `.dialog__${elementName}`
      var elementDateXpathSelector = `//div[contains(@class, 'dialog__${elementName}')]//div[contains(@class, 'date-picker-table--date')]//div[contains(text(), '${date}')]`
      var elementOKBtnXpathSelector = `//div[contains(@class, 'dialog__${elementName}')]//div[contains(text(), 'OK')]`
      this
        .click(elementInputSelector)
        .expect.element(elementDialogSelector).to.have.css('display').which.not.equals('none')
      this.api.pause(1500)
      this.api
        .useXpath()
        .expect.element(elementDateXpathSelector).to.be.visible
      this
        .click(elementDateXpathSelector)
        .click(elementOKBtnXpathSelector)
      this.api
        .useCss()
        .expect.element(elementDialogSelector).to.be.not.visible.before(2000)      
      // this.api
      //   .pause(1000)

      return this
    }
  },
  {
    clickPlaceAutocomplete: function(text) {
      this.api.pause(1000)
      .useXpath()
      .click(`//*[contains(text(), "${text}")]`)
      return this
    }
  }],
  elements: {}
};