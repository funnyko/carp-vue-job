module.exports = {
  commands: [{
    seachAndSelectPlace: function(element, searchTxt, selectTxt) {
      this.waitForElementVisible(element, 4000)
      .click(element)
      .waitForElementPresent('@placeAutocompleteInput', 500)
      .click('@placeAutocompleteInput')
      .setValue('@placeAutocompleteInput', searchTxt)
      .waitForElementPresent('@placeAutocompleteList', 5000)
      .clickPlaceAutocomplete(selectTxt)
    }
  },
  {
    clickPlaceAutocomplete: function(text) {
      this.api.pause(1000)
      .useXpath()
      .click(`//*[contains(text(), "${text}")]`)
      return this
    }
  }],
  elements: {
    startInput: {
      selector: '#input__place-start'
    },
    destinationInput: {
      selector: '#input__place-destination'
    },
    placeAutocompleteInput: {
      selector: '.input__place-autocomplete > div > input'
    },
    placeAutocompleteList: {
      selector: '.list__place-autocomplete'
    },
  }
};