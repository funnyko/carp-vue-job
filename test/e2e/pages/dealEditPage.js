module.exports = {
  url: function() { 
    return this.api.globals.prodServerURL + '/editdeal/PwGygfyVvkNA4URb4W3I'; 
  },
  commands: [],
  sections: {
    makeDealDialog: {
      selector: '.dialog__make-deal',
      elements: {
        messageInput: {
          selector: '#input__deal-message'
        },
        priceInput: {
          selector: '#input__deal-price'
        },
        pickupTimeInput: {
          selector: '#input__pickup-time'
        },
        arrivalTimeInput: {
          selector: '#input__arrival-time'
        },
        submitBtn: {
          selector: '.btn__submit'
        }
      }
    }
  },
  elements: {
    makeDealBtn: {
      selector: '#btn__make-deal'
    }
  }
};